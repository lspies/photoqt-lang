<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="243"/>
        <source>navigation</source>
        <extracomment>This is a category in the main menu.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="262"/>
        <source>previous</source>
        <extracomment>as in: PREVIOUS image. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="275"/>
        <source>next</source>
        <extracomment>as in: NEXT image. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="291"/>
        <source>first</source>
        <extracomment>as in: FIRST image. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="301"/>
        <source>last</source>
        <extracomment>as in: LAST image. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="312"/>
        <source>Browse images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="322"/>
        <source>Map Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="346"/>
        <source>current image</source>
        <extracomment>This is a category in the main menu.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="373"/>
        <source>Zoom</source>
        <extracomment>Entry in main menu. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="411"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="473"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="535"/>
        <source>reset</source>
        <extracomment>Used as in RESET zoom.
----------
Used as in RESET rotation.
----------
Used as in RESET mirroring.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="424"/>
        <source>Enable to preserve zoom levels across images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="445"/>
        <source>Rotation</source>
        <extracomment>Entry in main menu. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="486"/>
        <source>Enable to preserve rotation angle across images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="507"/>
        <source>Mirror</source>
        <extracomment>Mirroring (or flipping) an image. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="548"/>
        <source>Enable to preserve mirror across images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="559"/>
        <source>Hide histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="559"/>
        <source>Show histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="568"/>
        <source>Hide current location</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="570"/>
        <source>Show current location</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="594"/>
        <source>all images</source>
        <extracomment>This is a category in the main menu.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="621"/>
        <source>Slideshow</source>
        <extracomment>Entry in main menu. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="631"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="676"/>
        <source>Start</source>
        <extracomment>Used as in START slideshow. Please keep short
----------
Used as in START advanced sort. Please keep short</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="642"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="687"/>
        <source>Setup</source>
        <extracomment>Used as in SETUP slideshow. Please keep short
----------
Used as in SETUP advanced sort. Please keep short</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="666"/>
        <source>Sort</source>
        <extracomment>Entry in main menu. Please keep short.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="701"/>
        <source>Filter images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="711"/>
        <source>Streaming (Chromecast)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="720"/>
        <source>Open in default file manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="744"/>
        <source>general</source>
        <extracomment>This is a category in the main menu.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="761"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="770"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="783"/>
        <source>Online help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="792"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="819"/>
        <source>custom</source>
        <extracomment>This is a category in the main menu.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="909"/>
        <source>Main menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="917"/>
        <source>Adjust height dynamically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="936"/>
        <source>Reset size to default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="657"/>
        <source>Thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="459"/>
        <source>Reset position to default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCImageFormats</name>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="66"/>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="79"/>
        <source>ERROR getting default image formats</source>
        <extracomment>This is the window title of an error message box</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
        <source>Not even a read-only version of the database of default image formats could be opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="67"/>
        <location filename="../cplusplus/singletons/engines/pqc_imageformats.cpp" line="80"/>
        <source>Something went terribly wrong somewhere!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCScriptsMetaData</name>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="89"/>
        <source>yes</source>
        <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="91"/>
        <source>no</source>
        <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="93"/>
        <source>No flash function</source>
        <extracomment>This string refers to the absence of a flash, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="95"/>
        <source>strobe return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="97"/>
        <source>strobe return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="99"/>
        <source>compulsory flash mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="101"/>
        <source>auto mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="103"/>
        <source>red-eye reduction mode</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="105"/>
        <source>return light detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="107"/>
        <source>return light not detected</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="155"/>
        <source>Invalid flash</source>
        <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="165"/>
        <source>Standard</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="168"/>
        <source>Landscape</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="171"/>
        <source>Portrait</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="174"/>
        <source>Night Scene</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="177"/>
        <source>Invalid Scene Type</source>
        <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="227"/>
        <source>Unknown</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="230"/>
        <source>Daylight</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="233"/>
        <source>Fluorescent</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="236"/>
        <source>Tungsten (incandescent light)</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="239"/>
        <source>Flash</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="242"/>
        <source>Fine weather</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="245"/>
        <source>Cloudy Weather</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="248"/>
        <source>Shade</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="251"/>
        <source>Daylight fluorescent</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="254"/>
        <source>Day white fluorescent</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="257"/>
        <source>Cool white fluorescent</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="260"/>
        <source>White fluorescent</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="263"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="266"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="269"/>
        <source>Standard light</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="282"/>
        <source>Other light source</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsmetadata.cpp" line="285"/>
        <source>Invalid light source</source>
        <extracomment>This string refers to the light source stored in image metadata</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCScriptsOther</name>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsother.cpp" line="106"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCScriptsUndo</name>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsundo.cpp" line="82"/>
        <source>File with original filename exists already</source>
        <comment>filemanagement</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsundo.cpp" line="96"/>
        <source>File restored from Trash</source>
        <comment>filemanagement</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsundo.cpp" line="103"/>
        <source>Failed to recover file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsundo.cpp" line="107"/>
        <source>Unknown action</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQCStartup</name>
    <message>
        <location filename="../cplusplus/other/pqc_startup.cpp" line="47"/>
        <source>SQLite error</source>
        <extracomment>This is the window title of an error message box</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_startup.cpp" line="48"/>
        <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQSettings</name>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="72"/>
        <source>ERROR getting database with default settings</source>
        <extracomment>This is the window title of an error message box</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
        <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="73"/>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
        <source>Something went terribly wrong somewhere!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="84"/>
        <source>ERROR opening database with default settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_settings.cpp" line="85"/>
        <source>I tried hard, but I just cannot open the database of default settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PQShortcuts</name>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="61"/>
        <source>ERROR getting database with default shortcuts</source>
        <extracomment>This is the window title of an error message box</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
        <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="62"/>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
        <source>Something went terribly wrong somewhere!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="73"/>
        <source>ERROR opening database with default settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/engines/pqc_shortcuts.cpp" line="74"/>
        <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="53"/>
        <source>Alt</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="55"/>
        <source>Ctrl</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="57"/>
        <source>Shift</source>
        <extracomment>Refers to a keyboard modifier</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="59"/>
        <source>Page Up</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="61"/>
        <source>Page Down</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="63"/>
        <source>Meta</source>
        <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="65"/>
        <source>Keypad</source>
        <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="67"/>
        <source>Escape</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="69"/>
        <source>Right</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="71"/>
        <source>Left</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="73"/>
        <source>Up</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="75"/>
        <source>Down</source>
        <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="77"/>
        <source>Space</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="79"/>
        <source>Delete</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="81"/>
        <source>Backspace</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="83"/>
        <source>Home</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="85"/>
        <source>End</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="87"/>
        <source>Insert</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="89"/>
        <source>Tab</source>
        <extracomment>Refers to one of the keys on the keyboard</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="91"/>
        <source>Return</source>
        <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="93"/>
        <source>Enter</source>
        <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="98"/>
        <source>Left Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="100"/>
        <source>Right Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="102"/>
        <source>Middle Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="104"/>
        <source>Back Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="106"/>
        <source>Forward Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="108"/>
        <source>Task Button</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="110"/>
        <source>Button #7</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="112"/>
        <source>Button #8</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="114"/>
        <source>Button #9</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="116"/>
        <source>Button #10</source>
        <extracomment>Refers to a mouse button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="118"/>
        <source>Double Click</source>
        <extracomment>Refers to a mouse event</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="120"/>
        <source>Wheel Up</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="122"/>
        <source>Wheel Down</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="124"/>
        <source>Wheel Left</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="126"/>
        <source>Wheel Right</source>
        <extracomment>Refers to the mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="128"/>
        <source>East</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="130"/>
        <source>South</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="132"/>
        <source>West</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="134"/>
        <source>North</source>
        <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>about</name>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="41"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="73"/>
        <source>Show configuration overview</source>
        <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="80"/>
        <source>License:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="85"/>
        <source>Open license in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="98"/>
        <source>Website:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="103"/>
        <source>Open website in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="111"/>
        <source>Contact:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="116"/>
        <source>Send an email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="163"/>
        <source>Configuration</source>
        <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAbout.qml" line="179"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>actions</name>
    <message>
        <location filename="../qml/actions/popout/PQExportPopout.qml" line="31"/>
        <source>Export image</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/popout/PQFileDialogPopout.qml" line="33"/>
        <location filename="../qml/filedialog/PQFileDialog.qml" line="87"/>
        <source>File Dialog</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/popout/PQMainMenuPopout.qml" line="33"/>
        <source>Main Menu</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/popout/PQMetaDataPopout.qml" line="33"/>
        <source>Metadata</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/popout/PQAboutPopout.qml" line="31"/>
        <source>About</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/popout/PQMapExplorerPopout.qml" line="32"/>
        <location filename="../qml/actions/PQMapExplorer.qml" line="56"/>
        <source>Map Explorer</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>advancedsort</name>
    <message>
        <location filename="../qml/actions/popout/PQAdvancedSortPopout.qml" line="32"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="42"/>
        <source>Advanced image sort</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="44"/>
        <source>Sort images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="83"/>
        <source>Sorting criteria:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="90"/>
        <source>Resolution</source>
        <extracomment>The image resolution (width/height in pixels)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="92"/>
        <source>Dominant color</source>
        <extracomment>The color that is most common in the image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="94"/>
        <source>Average color</source>
        <extracomment>the average color of the image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="96"/>
        <source>Luminosity</source>
        <extracomment>the average color of the image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="97"/>
        <source>Exif date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="154"/>
        <source>Sort by image resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="171"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="237"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="333"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="426"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="520"/>
        <source>in ascending order</source>
        <extracomment>as is: sort in ascending order</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="177"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="242"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="338"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="431"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="525"/>
        <source>in descending order</source>
        <extracomment>as is: sort in descending order</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="221"/>
        <source>Sort by dominant color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="258"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="353"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="446"/>
        <source>low quality (fast)</source>
        <extracomment>quality and speed of advanced sorting of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="260"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="354"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="447"/>
        <source>medium quality</source>
        <extracomment>quality and speed of advanced sorting of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="262"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="355"/>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="448"/>
        <source>high quality (slow)</source>
        <extracomment>quality and speed of advanced sorting of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="317"/>
        <source>Sort by average color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="410"/>
        <source>Sort by luminosity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="504"/>
        <source>Sort by date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="537"/>
        <source>Order of priority:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="550"/>
        <source>Exif tag: Original date/time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="551"/>
        <source>Exif tag: Digitized date/time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="552"/>
        <source>File creation date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="553"/>
        <source>File modification date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQAdvancedSort.qml" line="631"/>
        <source>If a value cannot be found, PhotoQt will proceed to the next item in the list.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>buttongeneric</name>
    <message>
        <location filename="../qml/elements/PQButton.qml" line="60"/>
        <location filename="../qml/elements/PQButtonElement.qml" line="44"/>
        <source>Ok</source>
        <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQButton.qml" line="62"/>
        <location filename="../qml/elements/PQButtonElement.qml" line="46"/>
        <source>Cancel</source>
        <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQButton.qml" line="64"/>
        <location filename="../qml/elements/PQButtonElement.qml" line="48"/>
        <source>Save</source>
        <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQButton.qml" line="66"/>
        <location filename="../qml/elements/PQButtonElement.qml" line="50"/>
        <source>Close</source>
        <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQButton.qml" line="112"/>
        <location filename="../qml/elements/PQButtonElement.qml" line="106"/>
        <location filename="../qml/elements/PQButtonIcon.qml" line="112"/>
        <source>Activate button</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>commandlineparser</name>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="37"/>
        <source>Image Viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="39"/>
        <source>Image file to open.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="46"/>
        <source>Make PhotoQt ask for a new file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="48"/>
        <source>Shows PhotoQt from system tray.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="50"/>
        <source>Hides PhotoQt to system tray.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="52"/>
        <source>Quit PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="54"/>
        <source>Show/Hide PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="56"/>
        <source>Enable system tray icon.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="58"/>
        <source>Disable system tray icon.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="60"/>
        <source>Start PhotoQt hidden to the system tray.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="62"/>
        <source>Simulate a shortcut sequence</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="64"/>
        <source>Change setting to specified value.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="66"/>
        <source>settingname:value</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="68"/>
        <source>Switch on debug messages.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="70"/>
        <source>Switch off debug messages.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="72"/>
        <source>Export configuration to given filename.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="74"/>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="78"/>
        <source>filename</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="76"/>
        <source>Import configuration from given filename.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="80"/>
        <source>Check the configuration and correct any detected issues.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="82"/>
        <source>Reset default configuration.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/other/pqc_commandlineparser.cpp" line="84"/>
        <source>Show configuration overview.</source>
        <extracomment>Command line option</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>contextmenu</name>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="52"/>
        <source>Rename file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="60"/>
        <source>Copy file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="68"/>
        <source>Move file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="76"/>
        <source>Delete file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="87"/>
        <source>Manipulate image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="91"/>
        <source>Scale image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="99"/>
        <source>Crop image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="107"/>
        <source>Tag faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="119"/>
        <source>Use image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="123"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="131"/>
        <source>Export to different format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="139"/>
        <source>Set as wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="149"/>
        <source>Hide QR/barcodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="149"/>
        <source>Detect QR/barcodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="161"/>
        <source>About image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="165"/>
        <source>Show histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="174"/>
        <source>Show on map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="184"/>
        <source>Select color profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="189"/>
        <source>Default color profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="227"/>
        <source>Manage PhotoQt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="231"/>
        <source>Browse images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="238"/>
        <source>Map Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="245"/>
        <source>Open settings manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQContextMenu.qml" line="252"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>crop</name>
    <message>
        <location filename="../qml/actions/popout/PQCropPopout.qml" line="31"/>
        <location filename="../qml/actions/PQCrop.qml" line="43"/>
        <source>Crop image</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCrop.qml" line="45"/>
        <location filename="../qml/actions/PQCrop.qml" line="382"/>
        <source>Crop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>export</name>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="46"/>
        <source>Export image</source>
        <extracomment>title of action element</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="49"/>
        <location filename="../qml/actions/PQExport.qml" line="56"/>
        <source>Export</source>
        <extracomment>written on button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="91"/>
        <source>Something went wrong during export to the selected format...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="122"/>
        <source>Favorites:</source>
        <extracomment>These are the favorite image formats for exporting images to</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="135"/>
        <source>no favorites set</source>
        <extracomment>the favorites are image formats for exporting images to</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="220"/>
        <location filename="../qml/actions/PQExport.qml" line="366"/>
        <source>Click to select this image format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="242"/>
        <location filename="../qml/actions/PQExport.qml" line="388"/>
        <source>Click to remove this image format from your favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="389"/>
        <source>Click to add this image format to your favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQExport.qml" line="415"/>
        <source>Selected target format:</source>
        <extracomment>The target format is the format the image is about to be exported to</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>facetagging</name>
    <message>
        <location filename="../qml/image/imageitems/PQPhotoSphere.qml" line="467"/>
        <source>Click to exit photo sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="72"/>
        <source>Click to exit face tagging mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="231"/>
        <source>Who is this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="345"/>
        <source>Tagging faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="345"/>
        <source>Face tagging mode activated. Click-and-drag to tag faces.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>filedialog</name>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="203"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="814"/>
        <source>File size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="204"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="815"/>
        <source>File type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="205"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="781"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="816"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="206"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="782"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="817"/>
        <source>Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="208"/>
        <source>Location:</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="113"/>
        <source>Go backwards in history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="119"/>
        <source>Go forwards in history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="125"/>
        <source>Go up a level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="150"/>
        <source>Show files as icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="172"/>
        <source>Show files as list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="207"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="268"/>
        <source>Edit location</source>
        <extracomment>The location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="365"/>
        <source>Navigate to this location</source>
        <extracomment>The location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="424"/>
        <source>no subfolders found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQBreadCrumbs.qml" line="631"/>
        <source>Click to edit location</source>
        <extracomment>The location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="195"/>
        <source>no supported files/folders found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="587"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="589"/>
        <source>%1 image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="587"/>
        <location filename="../qml/filedialog/PQFileView.qml" line="591"/>
        <source>%1 images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="780"/>
        <source># images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1256"/>
        <source>Open this folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1265"/>
        <source>Add to Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1276"/>
        <source>Load content of folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1276"/>
        <source>Load this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1288"/>
        <source>Load all selected files/folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1288"/>
        <source>Load all selected folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1343"/>
        <source>Remove file selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1343"/>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1354"/>
        <source>Remove all file selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1354"/>
        <source>Select all files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1366"/>
        <source>Delete selection permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1366"/>
        <source>Delete selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1367"/>
        <source>Delete file permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1367"/>
        <source>Delete file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1368"/>
        <source>Delete folder permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1368"/>
        <source>Delete folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1369"/>
        <source>Delete file/folder permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1369"/>
        <source>Delete file/folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1376"/>
        <source>Cut selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1377"/>
        <source>Cut file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1378"/>
        <source>Cut folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1379"/>
        <source>Cut file/folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1386"/>
        <source>Copy selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1387"/>
        <source>Copy file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1388"/>
        <source>Copy folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1389"/>
        <source>Copy file/folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1395"/>
        <source>Paste files from clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1415"/>
        <source>Show hidden files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1423"/>
        <source>Show tooltip with image details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="57"/>
        <source>%1 files already exist in the target directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="62"/>
        <source>Check the files below that you want to paste anyways. Files left unchecked will not be pasted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="186"/>
        <source>Select all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPasteExistingConfirm.qml" line="193"/>
        <source>Select none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="94"/>
        <source>bookmarks and devices disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="437"/>
        <source>Show entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="437"/>
        <source>Hide entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="455"/>
        <source>Remove entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="473"/>
        <source>Hide hidden entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="473"/>
        <source>Show hidden entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="499"/>
        <source>Hide bookmarked places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="499"/>
        <source>Show bookmarked places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="505"/>
        <source>Hide storage devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="505"/>
        <source>Show storage devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="535"/>
        <source>Storage Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQPlaces.qml" line="558"/>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="332"/>
        <source>Bookmarks</source>
        <extracomment>file manager settings popdown: menu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="43"/>
        <source>View</source>
        <extracomment>file manager settings popdown: menu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="49"/>
        <source>layout</source>
        <extracomment>file manager settings popdown: submenu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="54"/>
        <source>list view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="65"/>
        <source>icon view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="81"/>
        <source>drag and drop</source>
        <extracomment>file manager settings popdown: submenu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="86"/>
        <source>enable for list view</source>
        <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="96"/>
        <source>enable for grid view</source>
        <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="106"/>
        <source>enable for bookmarks</source>
        <extracomment>file manager settings popdown: the thing to enable here is drag-and-drop</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="120"/>
        <source>padding</source>
        <extracomment>file manager settings popdown: submenu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="142"/>
        <source>select with single click</source>
        <extracomment>file manager settings popdown: how to select files</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="152"/>
        <source>remember selections</source>
        <extracomment>file manager settings popdown: how to select files</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="161"/>
        <source>hidden files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="170"/>
        <source>tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="180"/>
        <source>Remember last location</source>
        <extracomment>The location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="191"/>
        <source>Thumbnails</source>
        <extracomment>file manager settings popdown: menu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="195"/>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="225"/>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="371"/>
        <source>show</source>
        <extracomment>file manager settings popdown: show thumbnails
----------
file manager settings popdown: show folder thumbnails
----------
file manager settings popdown: show image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="205"/>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="235"/>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="414"/>
        <source>scale and crop</source>
        <extracomment>file manager settings popdown: scale and crop the thumbnails
----------
file manager settings popdown: scale and crop the folder thumbnails
----------
file manager settings popdown: scale and crop image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="220"/>
        <source>folder thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="246"/>
        <source>autoload</source>
        <extracomment>file manager settings popdown: automatically load the folder thumbnails</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="257"/>
        <source>loop</source>
        <extracomment>file manager settings popdown: loop through the folder thumbnails</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="273"/>
        <source>timeout</source>
        <extracomment>file manager settings popdown: timeout between switching folder thumbnails</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="296"/>
        <source>1 second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="314"/>
        <source>half a second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="334"/>
        <source>show bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="344"/>
        <source>show devices</source>
        <extracomment>file manager settings popdown: the devices here are the storage devices</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="354"/>
        <source>show temporary devices</source>
        <extracomment>file manager settings popdown: the devices here are the storage devices</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="367"/>
        <source>Preview</source>
        <extracomment>file manager settings popdown: menu title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="381"/>
        <source>higher resolution</source>
        <extracomment>file manager settings popdown: use higher resolution for image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="392"/>
        <source>blur</source>
        <extracomment>file manager settings popdown: blur image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="403"/>
        <source>mute colors</source>
        <extracomment>file manager settings popdown: mute the colors in image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQSettingsMenu.qml" line="426"/>
        <source>color intensity</source>
        <extracomment>file manager settings popdown: color intensity of image previews</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="67"/>
        <source>Zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="71"/>
        <source>Adjust size of files and folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="139"/>
        <source>Cancel and close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="158"/>
        <source>Sort by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="169"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="170"/>
        <source>Natural Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="171"/>
        <source>Time modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="172"/>
        <source>File size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="173"/>
        <source>File type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="174"/>
        <source>reverse order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="249"/>
        <location filename="../qml/filedialog/PQTweaks.qml" line="282"/>
        <location filename="../qml/filedialog/PQTweaks.qml" line="506"/>
        <location filename="../qml/filedialog/PQTweaks.qml" line="573"/>
        <source>All supported images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/filedialog/PQTweaks.qml" line="382"/>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>filemanagement</name>
    <message>
        <location filename="../qml/actions/popout/PQDeletePopout.qml" line="31"/>
        <location filename="../qml/actions/PQDelete.qml" line="43"/>
        <source>Delete file?</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/popout/PQRenamePopout.qml" line="31"/>
        <location filename="../qml/actions/PQRename.qml" line="42"/>
        <location filename="../qml/actions/PQRename.qml" line="45"/>
        <source>Rename file</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="773"/>
        <source>File successfully deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="773"/>
        <location filename="../qml/other/PQShortcuts.qml" line="783"/>
        <location filename="../qml/other/PQShortcuts.qml" line="793"/>
        <location filename="../qml/other/PQShortcuts.qml" line="827"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="776"/>
        <source>Could not delete file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="776"/>
        <location filename="../qml/other/PQShortcuts.qml" line="786"/>
        <location filename="../qml/other/PQShortcuts.qml" line="796"/>
        <source>Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="783"/>
        <location filename="../qml/other/PQShortcuts.qml" line="793"/>
        <source>File successfully moved to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="786"/>
        <location filename="../qml/other/PQShortcuts.qml" line="796"/>
        <source>Could not move file to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="823"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="823"/>
        <source>Nothing to restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="825"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCopy.qml" line="55"/>
        <source>Copy here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCopy.qml" line="97"/>
        <location filename="../qml/actions/PQMove.qml" line="96"/>
        <source>An error occured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCopy.qml" line="102"/>
        <source>File could not be copied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCrop.qml" line="400"/>
        <location filename="../qml/actions/PQScale.qml" line="439"/>
        <source>Action not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCrop.qml" line="400"/>
        <source>This image can not be cropped.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQDelete.qml" line="45"/>
        <location filename="../qml/actions/PQDelete.qml" line="113"/>
        <source>Move to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQDelete.qml" line="49"/>
        <location filename="../qml/actions/PQDelete.qml" line="115"/>
        <source>Delete permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQDelete.qml" line="79"/>
        <source>Are you sure you want to delete this file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQDelete.qml" line="100"/>
        <source>An error occured, file could not be deleted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQMove.qml" line="56"/>
        <source>Move here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQMove.qml" line="101"/>
        <source>File could not be moved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQRename.qml" line="63"/>
        <source>old filename:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQRename.qml" line="87"/>
        <source>new filename:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQRename.qml" line="140"/>
        <source>An error occured, file could not be renamed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQRename.qml" line="152"/>
        <source>A file with this filename already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="439"/>
        <source>This image can not be scaled.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>filter</name>
    <message>
        <location filename="../qml/actions/popout/PQFilterPopout.qml" line="31"/>
        <location filename="../qml/actions/PQFilter.qml" line="39"/>
        <source>Filter images in current directory</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="42"/>
        <source>Filter</source>
        <extracomment>Written on a clickable button - please keep short</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="49"/>
        <source>Remove filter</source>
        <extracomment>Written on a clickable button - please keep short</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="94"/>
        <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="123"/>
        <source>File name/extension:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="147"/>
        <source>Enter terms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="161"/>
        <source>Image Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="179"/>
        <location filename="../qml/actions/PQFilter.qml" line="266"/>
        <source>greater than</source>
        <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="181"/>
        <location filename="../qml/actions/PQFilter.qml" line="268"/>
        <source>less than</source>
        <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="248"/>
        <source>File size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQFilter.qml" line="328"/>
        <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>histogram</name>
    <message>
        <location filename="../qml/ongoing/popout/PQHistogramPopout.qml" line="33"/>
        <location filename="../qml/ongoing/PQHistogram.qml" line="224"/>
        <source>Histogram</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="88"/>
        <source>Click-and-drag to move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="196"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="210"/>
        <source>Error loading histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="274"/>
        <source>show histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="287"/>
        <source>RGB colors</source>
        <extracomment>used in context menu for histogram</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="299"/>
        <source>gray scale</source>
        <extracomment>used in context menu for histogram</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQHistogram.qml" line="312"/>
        <source>Hide histogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="449"/>
        <source>show quick actions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>image</name>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="363"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="317"/>
        <source>Click to enter viewer mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="413"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="367"/>
        <source>Lock left/right arrow keys to page navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="323"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="277"/>
        <source>Page %1/%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="197"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="155"/>
        <source>Go to first page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="227"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="185"/>
        <source>Go to previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="257"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="215"/>
        <source>Go to next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="288"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="246"/>
        <source>Go to last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="441"/>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="508"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="395"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="462"/>
        <location filename="../qml/image/components/PQImageAnimatedControls.qml" line="288"/>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="198"/>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="257"/>
        <location filename="../qml/image/components/PQVideoControls.qml" line="462"/>
        <source>Hide controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="480"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="434"/>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="237"/>
        <location filename="../qml/image/components/PQVideoControls.qml" line="442"/>
        <source>Arrow keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="490"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="444"/>
        <source>Viewer mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="110"/>
        <source>Click and drag to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="140"/>
        <source>Click to play/pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="229"/>
        <source>Volume:</source>
        <extracomment>The volume here is referring to SOUND volume</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="231"/>
        <source>Click to mute/unmute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="305"/>
        <source>Lock left/right arrow keys to jumping forwards/backwards 5 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="334"/>
        <source>Click to always show video controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="335"/>
        <source>Click to hide video controls when video is playing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="421"/>
        <source>Pause video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="421"/>
        <source>Play video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQVideoControls.qml" line="431"/>
        <source>Mute</source>
        <extracomment>refers to muting sound</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="141"/>
        <location filename="../qml/image/components/PQMinimapPopout.qml" line="34"/>
        <source>Minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="201"/>
        <source>Small minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="207"/>
        <source>Normal minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="213"/>
        <source>Large minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="219"/>
        <source>Very large minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQMinimap.qml" line="227"/>
        <source>Hide minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="172"/>
        <source>Lock arrow keys to moving photo sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="463"/>
        <location filename="../qml/image/components/PQArchiveControls.qml" line="500"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="417"/>
        <location filename="../qml/image/components/PQDocumentControls.qml" line="454"/>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="220"/>
        <location filename="../qml/image/components/PQPhotoSphereControls.qml" line="249"/>
        <location filename="../qml/image/components/PQVideoControls.qml" line="355"/>
        <location filename="../qml/image/components/PQVideoControls.qml" line="454"/>
        <source>Reset position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQBarCodes.qml" line="56"/>
        <source>Nothing found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQBarCodes.qml" line="56"/>
        <source>No bar/QR codes found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQBarCodes.qml" line="58"/>
        <location filename="../qml/image/components/PQBarCodes.qml" line="60"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQBarCodes.qml" line="58"/>
        <source>1 bar/QR code found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQBarCodes.qml" line="60"/>
        <source>%1 bar/QR codes found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQImageAnimatedControls.qml" line="218"/>
        <source>Save current frame to new file</source>
        <extracomment>The frame here refers to one of the images making up an animation of a gif or other animated image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQImageAnimatedControls.qml" line="264"/>
        <source>Lock left/right arrow keys to frame navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/imageitems/PQImageNormal.qml" line="264"/>
        <source>Click here to enter photo sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/imageitems/PQImageNormal.qml" line="472"/>
        <source>Toggle autoplay</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>imageprovider</name>
    <message>
        <location filename="../cplusplus/images/provider/pqc_providerfull.cpp" line="55"/>
        <location filename="../cplusplus/images/provider/pqc_providerthumb.cpp" line="204"/>
        <source>File failed to load, it does not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsimages.cpp" line="1816"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsimages.cpp" line="1819"/>
        <source>Application of color profile failed for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptsimages.cpp" line="1817"/>
        <source>Application of color profiles failed repeatedly. Support for color spaces will be disabled, but can be enabled again in the settings manager.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>imgur</name>
    <message>
        <location filename="../qml/actions/popout/PQImgurPopout.qml" line="32"/>
        <location filename="../qml/actions/PQImgur.qml" line="43"/>
        <location filename="../qml/actions/PQImgur.qml" line="44"/>
        <source>Upload to imgur.com</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="51"/>
        <source>Show past uploads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="100"/>
        <source>This seems to take a long time...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="101"/>
        <source>There might be a problem with your internet connection or the imgur.com servers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="114"/>
        <source>An Error occurred while uploading image!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="115"/>
        <source>Error code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="131"/>
        <source>You do not seem to be connected to the internet...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="132"/>
        <source>Unable to upload!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="178"/>
        <source>Access Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="193"/>
        <location filename="../qml/actions/PQImgur.qml" line="231"/>
        <location filename="../qml/actions/PQImgur.qml" line="399"/>
        <location filename="../qml/actions/PQImgur.qml" line="442"/>
        <source>Click to open in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="216"/>
        <source>Delete Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="294"/>
        <source>No past uploads found</source>
        <extracomment>The uploads are uploads to imgur.com</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="390"/>
        <source>Access:</source>
        <extracomment>Used as in: access this image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="433"/>
        <source>Delete:</source>
        <extracomment>Used as in: delete this image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQImgur.qml" line="494"/>
        <source>Clear all</source>
        <extracomment>Written on button, please keep short. Used as in: clear all entries</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>logging</name>
    <message>
        <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="107"/>
        <source>enable</source>
        <extracomment>Used as in: enable debug message</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="139"/>
        <source>copy to clipboard</source>
        <extracomment>the thing being copied here are the debug messages</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/popout/PQLoggingPopout.qml" line="145"/>
        <source>save to file</source>
        <extracomment>the thing saved to files here are the debug messages</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mapcurrent</name>
    <message>
        <location filename="../qml/ongoing/popout/PQMapCurrentPopout.qml" line="33"/>
        <location filename="../qml/ongoing/PQMapCurrent.qml" line="215"/>
        <source>Current location</source>
        <extracomment>Window title
----------
The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMapCurrent.qml" line="89"/>
        <source>Click-and-drag to move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMapCurrent.qml" line="194"/>
        <source>No location data</source>
        <extracomment>The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>mapexplorer</name>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="46"/>
        <source>Image zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImagesTweaks.qml" line="69"/>
        <source>scale and crop thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="49"/>
        <source>Map zoom:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMapTweaks.qml" line="75"/>
        <source>Reset view</source>
        <extracomment>The view here is the map layout in the map explorer</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="255"/>
        <source>Zoom to location</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="263"/>
        <source>Load image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="274"/>
        <source>Copy location to clipboard</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="292"/>
        <source>no images in currently visible area</source>
        <extracomment>the currently visible area refers to the latitude/longitude selection in the map explorer</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerImages.qml" line="305"/>
        <source>no images with location data in current folder</source>
        <extracomment>The location here is the GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMap.qml" line="200"/>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMap.qml" line="498"/>
        <source>Copy location to clipboard:</source>
        <extracomment>The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/mapexplorerparts/PQMapExplorerMap.qml" line="531"/>
        <source>Click to show a menu for copying location to clipboard.</source>
        <extracomment>The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>metadata</name>
    <message>
        <location filename="../qml/elements/PQMetaDataEntry.qml" line="38"/>
        <source>Copy value to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="216"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="238"/>
        <location filename="../qml/ongoing/PQMetaData.qml" line="559"/>
        <source>Metadata</source>
        <extracomment>The title of the floating element</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="280"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="286"/>
        <source>Dimensions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="292"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="298"/>
        <source>File size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="304"/>
        <source>File type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="315"/>
        <source>Make</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="321"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="327"/>
        <source>Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="338"/>
        <source>Time Photo was Taken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="344"/>
        <source>Exposure Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="350"/>
        <source>Flash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="362"/>
        <source>Scene Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="368"/>
        <source>Focal Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="374"/>
        <source>F Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="380"/>
        <source>Light Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="391"/>
        <source>Keywords</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="398"/>
        <source>Location</source>
        <extracomment>The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="404"/>
        <source>Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="415"/>
        <source>GPS Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="419"/>
        <source>Click to copy value to clipboard, Ctrl+Click to open location in online map service</source>
        <extracomment>The location here is a GPS location</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="647"/>
        <source>Adjust height dynamically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="668"/>
        <source>Reset size to default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="394"/>
        <source>Window buttons</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>navigate</name>
    <message>
        <location filename="../qml/ongoing/PQNavigation.qml" line="84"/>
        <source>Click and drag to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQNavigation.qml" line="122"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="112"/>
        <source>Navigate to previous image in folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQNavigation.qml" line="156"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="138"/>
        <source>Navigate to next image in folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQNavigation.qml" line="187"/>
        <source>Show main menu</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>other</name>
    <message>
        <location filename="../qml/other/PQBackgroundMessage.qml" line="41"/>
        <source>Thumbnails</source>
        <extracomment>Label shown at startup before a file is loaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQBackgroundMessage.qml" line="43"/>
        <source>Main menu</source>
        <extracomment>Label shown at startup before a file is loaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQBackgroundMessage.qml" line="45"/>
        <source>Metadata</source>
        <extracomment>Label shown at startup before a file is loaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQBackgroundMessage.qml" line="122"/>
        <source>Open a file</source>
        <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>popinpopout</name>
    <message>
        <location filename="../qml/actions/PQMapExplorer.qml" line="167"/>
        <location filename="../qml/elements/PQTemplateFloating.qml" line="190"/>
        <location filename="../qml/elements/PQTemplateFullscreen.qml" line="274"/>
        <location filename="../qml/filedialog/PQFileDialog.qml" line="247"/>
        <location filename="../qml/image/components/PQMinimap.qml" line="325"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="1068"/>
        <location filename="../qml/ongoing/PQMetaData.qml" line="729"/>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="346"/>
        <source>Merge into main interface</source>
        <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQMapExplorer.qml" line="169"/>
        <location filename="../qml/elements/PQTemplateFloating.qml" line="192"/>
        <location filename="../qml/elements/PQTemplateFullscreen.qml" line="276"/>
        <location filename="../qml/filedialog/PQFileDialog.qml" line="249"/>
        <location filename="../qml/image/components/PQMinimap.qml" line="327"/>
        <location filename="../qml/ongoing/PQMainMenu.qml" line="1070"/>
        <location filename="../qml/ongoing/PQMetaData.qml" line="731"/>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="348"/>
        <source>Move to its own window</source>
        <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>quickactions</name>
    <message>
        <location filename="../qml/ongoing/popout/PQQuickActionsPopout.qml" line="33"/>
        <source>Quick Actions</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="92"/>
        <source>Click-and-drag to move.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="110"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1330"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1532"/>
        <source>Rename file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="111"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1331"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1533"/>
        <source>Copy file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="112"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1332"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1534"/>
        <source>Move file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="113"/>
        <source>Delete file (with confirmation)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="114"/>
        <source>Move file directly to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="115"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1334"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1536"/>
        <source>Rotate left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="116"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1335"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1537"/>
        <source>Rotate right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="117"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1336"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1538"/>
        <source>Mirror horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="118"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1337"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1539"/>
        <source>Mirror vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="119"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1338"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1540"/>
        <source>Crop image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="120"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1339"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1541"/>
        <source>Scale image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="121"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1340"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1542"/>
        <source>Tag faces</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="122"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1341"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1543"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="123"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1342"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1544"/>
        <source>Export to different format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="124"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1343"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1545"/>
        <source>Set as wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="126"/>
        <source>Hide QR/barcodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="127"/>
        <source>Detect QR/barcodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="128"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1345"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1547"/>
        <source>Close window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="129"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1346"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1548"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="245"/>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="389"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1329"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1549"/>
        <source>separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1333"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1535"/>
        <source>Delete file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1344"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1546"/>
        <source>Detect/hide QR/barcodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>quickinfo</name>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="171"/>
        <source>Click here to show main menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="214"/>
        <source>Click here to not keep window in foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="214"/>
        <source>Click here to keep window in foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="249"/>
        <source>Click here to minimize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="287"/>
        <source>Click here to restore window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="288"/>
        <source>Click here to maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="324"/>
        <source>Click here to enter fullscreen mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="325"/>
        <source>Click here to exit fullscreen mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="362"/>
        <source>Click here to close PhotoQt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>scale</name>
    <message>
        <location filename="../qml/actions/popout/PQScalePopout.qml" line="31"/>
        <location filename="../qml/actions/PQScale.qml" line="43"/>
        <source>Scale image</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQCrop.qml" line="196"/>
        <location filename="../qml/actions/PQScale.qml" line="75"/>
        <source>An error occured, file could not be scaled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="45"/>
        <location filename="../qml/actions/PQScale.qml" line="410"/>
        <source>Scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="94"/>
        <source>Width:</source>
        <extracomment>The number of horizontal pixels of the image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="100"/>
        <source>Height:</source>
        <extracomment>The number of vertical pixels of the image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="186"/>
        <source>New size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="189"/>
        <source>pixels</source>
        <extracomment>This is used as in: 100x100 pixels</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQScale.qml" line="277"/>
        <source>Quality:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>settingsmanager</name>
    <message>
        <location filename="../qml/settingsmanager/popout/PQSettingsManagerPopout.qml" line="33"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="41"/>
        <source>Settings Manager</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="55"/>
        <source>Apply changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="59"/>
        <source>Revert changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="81"/>
        <source>auto-save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="89"/>
        <source>compact</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="110"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="77"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="319"/>
        <source>Interface</source>
        <extracomment>A settings category
----------
A settings subcategory and the qml filename
----------
This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="78"/>
        <source>Language</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="116"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="227"/>
        <source>Fullscreen or window mode</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="117"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="350"/>
        <source>Window buttons</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="118"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="581"/>
        <source>Accent color</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="119"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="826"/>
        <source>Font weight</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="120"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="940"/>
        <source>Notification</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="142"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="143"/>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="86"/>
        <source>Background</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="163"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="164"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="152"/>
        <source>Popout</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="165"/>
        <source>Keep popouts open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="166"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="467"/>
        <source>Pop out when window is small</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="194"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="195"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="100"/>
        <source>Edges</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="196"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="289"/>
        <source>Sensitivity</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="204"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="205"/>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="91"/>
        <source>Context menu</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="206"/>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="356"/>
        <source>Duplicate entries in main menu</source>
        <extracomment>The entries here are the custom entries in the context menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="210"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="211"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="81"/>
        <source>Status info</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="212"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="213"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="403"/>
        <source>Hide automatically</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="215"/>
        <source>Alignment</source>
        <extracomment>The alignment here refers to the position of the statusinfo, where along the top edge of the window it should be aligned along</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="216"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="580"/>
        <source>Window management</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="232"/>
        <source>Image view</source>
        <extracomment>A settings category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="235"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="335"/>
        <source>Image</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="236"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="90"/>
        <source>Margin</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="237"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="142"/>
        <source>Image size</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="238"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="248"/>
        <source>Transparency marker</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="239"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="293"/>
        <source>Interpolation</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="241"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="410"/>
        <source>Color profiles</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="256"/>
        <source>Interaction</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="258"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="268"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="118"/>
        <source>Minimap</source>
        <extracomment>Settings title. The minimap is a small version of the image used to show where the view is at.
----------
Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="259"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="337"/>
        <source>Mirror/Flip</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="260"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="381"/>
        <source>Floating navigation</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="273"/>
        <source>Folder</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="274"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="74"/>
        <source>Looping</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="275"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="119"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="75"/>
        <source>Sort images</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="276"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="526"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="223"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="85"/>
        <source>Animation</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="277"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="365"/>
        <source>Preloading</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="286"/>
        <source>Share online</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="291"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="79"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="88"/>
        <source>Metadata</source>
        <extracomment>A settings subcategory
----------
Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="292"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="132"/>
        <source>Labels</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="293"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="385"/>
        <source>Auto Rotation</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="294"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="429"/>
        <source>GPS map</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="295"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="499"/>
        <source>Floating element</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="296"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="557"/>
        <source>Face tags</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="297"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="659"/>
        <source>Look of face tags</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="332"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="494"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="75"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="591"/>
        <source>Thumbnails</source>
        <extracomment>A settings category
----------
Used as descriptor for a screen edge action
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="351"/>
        <source>All thumbnails</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="363"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="452"/>
        <source>Manage</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="378"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="381"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="382"/>
        <source>File types</source>
        <extracomment>A settings category
----------
A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="386"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="433"/>
        <source>Behavior</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="387"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="77"/>
        <source>PDF</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="473"/>
        <source>Configuration</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="481"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="79"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="320"/>
        <source>Other</source>
        <extracomment>This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="485"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="80"/>
        <source>File dialog</source>
        <extracomment>A settings subcategory
----------
Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="486"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="169"/>
        <source>Layout</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="487"/>
        <source>Show hidden files and folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="488"/>
        <source>Tooltip with Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="490"/>
        <source>Remember previous location</source>
        <extracomment>The location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="491"/>
        <source>Only select with single click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="492"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="459"/>
        <source>Sections</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="493"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="526"/>
        <source>Drag and drop</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="495"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="659"/>
        <source>Padding</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="496"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="710"/>
        <source>Folder thumbnails</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="497"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="839"/>
        <source>Preview</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="525"/>
        <source>Slideshow</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="527"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="254"/>
        <source>Interval</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="529"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="350"/>
        <source>Shuffle</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="531"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="451"/>
        <source>Include subfolders</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="532"/>
        <source>Music file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="621"/>
        <source>Ctrl+S = Apply changes, Ctrl+R = Revert changes, Esc = Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="717"/>
        <source>Unsaved changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="725"/>
        <source>The settings on this page have changed. Do you want to apply or discard them?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="737"/>
        <source>Apply</source>
        <extracomment>written on button, used as in: apply changes</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="755"/>
        <source>Discard</source>
        <extracomment>written on button, used as in: discard changes</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="79"/>
        <source>PhotoQt can show PDF and Postscript documents alongside your images, you can even enter a multi-page document and browse its pages as if they were images in a folder. The quality setting here - specified in dots per pixel (dpi) - affects the resolution and speed of loading such pages.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="388"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="139"/>
        <source>Archive</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="141"/>
        <source>PhotoQt allows the browsing of all images contained in an archive file (ZIP, RAR, etc.) as if they all are located in a folder. By default, PhotoQt uses Libarchive for this purpose, but for RAR archives in particular PhotoQt can call the external tool unrar to load and display the archive and its contents. Note that this requires unrar to be installed and located in your path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="143"/>
        <source>When an archive is loaded it is possible to browse through the contents of such a file either through floating controls that show up when the archive contains more than one file, or by entering the viewer mode. When the viewer mode is activated all files in the archive are loaded as thumbnails. The viewer mode can be activated by shortcut or through a small button located below the status info and as part of the floating controls.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="149"/>
        <source>use external tool: unrar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="389"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="233"/>
        <source>Video</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="171"/>
        <source>show floating controls for archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="96"/>
        <source>Escape key leaves document viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="178"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="492"/>
        <source>use left/right arrow to load previous/next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="184"/>
        <source>Escape key leaves archive viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="235"/>
        <source>PhotoQt can treat video files the same as image files, as long as the respective video formats are enabled. There are a few settings available for managing how videos behave in PhotoQt: Whether they should autoplay when loaded, whether they should loop from the beginning when the end is reached, whether to prefer libmpv (if available) or Qt for video playback, and which video thumbnail generator to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="242"/>
        <source>Autoplay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="528"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="249"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="305"/>
        <source>Loop</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="257"/>
        <source>prefer Qt Multimedia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="262"/>
        <source>prefer Libmpv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="278"/>
        <source>Video thumbnail generator:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="293"/>
        <source>Always use left/right arrow keys to jump back/ahead in videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="70"/>
        <source>Both Apple and Android devices can connect a short video clip to photos. Apple refers to this as Apple Live Photo, and Google refers to it as Motion Photo (or sometimes Micro Video). Apple stores small video files next to the image files that have the same filename but different file ending. Android embeds these video files in the image file. If the former is enabled, PhotoQt will hide the video files from the file list and automatically load them when the connected image file is loaded. If the latter is enabled PhotoQt will try to extract and show the video file once the respective image file is loaded. All of this is done asynchronously and should not cause any slowdown. PhotoQt can also show a small play/pause button in the bottom right corner of the window, and it can force the space bar to always play/pause the detected video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="78"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="173"/>
        <source>This feature is not supported by your build of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="87"/>
        <source>Look for Apple Live Photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="94"/>
        <source>Look for Google Motion Photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="106"/>
        <source>Show small play/pause/autoplay button in bottom right corner of window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="113"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="301"/>
        <source>Always use space key to play/pause videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="165"/>
        <source>PhotoQt can check whether the current image is a photo sphere by analyzing its metadata. If a equirectangular projection is detected, the photo sphere will be loaded instead of a flat image. In addition, the arrow keys can optionally be forced to be used for moving around the sphere regardless of which shortcut actions they are set to. Both partial photo spheres and 360 degree views are supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="188"/>
        <source>Enter photo spheres:</source>
        <extracomment>This is the info text for a combobox about how to enter photo spheres</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="195"/>
        <source>automatically</source>
        <extracomment>Used as in: Enter photo spheres automatically</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="197"/>
        <source>through central big button</source>
        <extracomment>Used as in: Enter photo spheres through central big button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="199"/>
        <source>never</source>
        <extracomment>Used as in: Enter photo spheres never</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="209"/>
        <source>show floating controls for photo spheres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="216"/>
        <source>use arrow keys for moving around photo spheres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="223"/>
        <source>Escape key leaves manually entered photo sphere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="230"/>
        <source>perform short panning animation after loading photo spheres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="390"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="364"/>
        <source>Animated images</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="366"/>
        <source>PhotoQt can show controls for animated images that allow for stepping through an animated image frame by frame, jumping to a specific frame, and play/pause the animation. Additionally is is possible to force the left/right arrow keys to load the previous/next frame and/or use the space key to play/pause the animation, no matter what shortcut action is set to these keys.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="373"/>
        <source>show floating controls for animated images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="392"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="476"/>
        <source>Documents</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="478"/>
        <source>When a document is loaded it is possible to navigate through the pages of such a file either through floating controls that show up when the document contains more than one page, or by entering the viewer mode. When the viewer mode is activated all pages are loaded as thumbnails. The viewer mode can be activated by shortcut or through a small button located below the status info and as part of the floating navigation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="485"/>
        <source>show floating controls for documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="380"/>
        <source>use left/right arrow to load previous/next frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="88"/>
        <source>quality:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="387"/>
        <source>Always use space key to play/pause animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="391"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="432"/>
        <source>RAW images</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="434"/>
        <source>Some RAW images have embedded thumbnail images. If available, PhotoQt will always use those for generating a thumbnail image. Some embedded thumbnails are even as large as the actual RAW image. In that case, PhotoQt can simply load those embedded images instead of the full RAW image. This can result in much faster load times.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQBehavior.qml" line="440"/>
        <source>use embedded image if available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="257"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="76"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="215"/>
        <source>Zoom</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="240"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="364"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="358"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="86"/>
        <source>Cache</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="92"/>
        <source>PhotoQt shows the main image fully stretched across its application window. For an improved visual experience, it can add a small margin of some pixels around the image to not have it stretch completely from edge to edge. Note that once an image is zoomed in the margin might be filled, it only applies to the default zoom level of an image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="101"/>
        <source>margin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="144"/>
        <source>PhotoQt ensures that an image is fully visible when first loaded. To achieve this, large images are zoomed out to fit into the view, but images smaller than the view are left as-is. Alternatively, large images can be loaded at full scale, and small images can be zoomed in to also fit into view. The latter option might result in small images appearing pixelated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="144"/>
        <source>In addition, PhotoQt by default scales the displayed images according to the scale factor of the screen so that images are displayed in their true size. If disabled then the main image will be scaled accordingly with the rest of the application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="156"/>
        <source>large images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="161"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="184"/>
        <source>fit to view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="165"/>
        <source>load at full scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="179"/>
        <source>small images:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="190"/>
        <source>load as-is</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="196"/>
        <source>respect scale factor of screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="250"/>
        <source>When an image contains transparent areas, then that area can be left transparent resulting in the background of PhotoQt to show. Alternatively, it is possible to show a checkerboard pattern behind the image, exposing the transparent areas of an image much clearer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="256"/>
        <source>show checkerboard pattern</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="295"/>
        <source>PhotoQt makes use of interpolation algorithms to show smooth lines and avoid potential artefacts to be shown. However, for small images this can lead to blurry images when no interpolation is necessary. Thus, for small images under the specified threshold PhotoQt can skip the use of interpolation algorithms. Note that both the width and height of an image need to be smaller than the threshold for it to be applied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="302"/>
        <source>disable interpolation for small images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="311"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="146"/>
        <source>threshold:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="360"/>
        <source>Whenever an image is loaded in full, PhotoQt caches such images in order to greatly improve performance if that same image is shown again soon after. This is done up to a certain memory limit after which the first images in the cache will be removed again to free up the required memory. Depending on the amount of memory available on the system, a higher value can lead to an improved user experience.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="369"/>
        <source>cache size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="412"/>
        <source>There are a variety of options available for handling color profiles. Depending on availability, PhotoQt can use a possibly embedded color profile or apply a custom selected default color profile, and it can offer a customized selection of color profiles through the context menu for choosing a different profile on-the-fly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="419"/>
        <source>Enable color profile management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="441"/>
        <source>Look for and load embedded color profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="453"/>
        <source>Change default color profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="468"/>
        <source>(no default color profile)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="483"/>
        <source>Select which color profiles should be offered through the context menu:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="499"/>
        <source>Filter color profiles</source>
        <extracomment>placeholder text in a text edit</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="630"/>
        <source>Remove imported color profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="692"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="311"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="331"/>
        <source>Select all</source>
        <extracomment>written on button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="701"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="320"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="340"/>
        <source>Select none</source>
        <extracomment>written on button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="710"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="329"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="349"/>
        <source>Invert</source>
        <extracomment>written on button, referring to inverting the selected options</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQImageSetIm.qml" line="726"/>
        <source>Import color profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="72"/>
        <source>It is possible to share an image from PhotoQt directly to imgur.com. This can either be done anonymously or to an imgur.com account. For the former, no setup is required, after a successful upload you are presented with the URL to access and the URL to delete the image. For the latter, PhotoQt first needs to be authenticated to an imgur.com user account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="80"/>
        <source>Note that any change here is saved immediately!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="94"/>
        <source>Authenticated with user account:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="107"/>
        <source>Authenticate</source>
        <extracomment>Written on button, used as in: Authenticate with user account</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="109"/>
        <source>Forget account</source>
        <extracomment>Written on button, used as in: Forget user account</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="152"/>
        <source>Switch to your browser and log into your imgur.com account. Then paste the displayed PIN in the field below. Click on the button above again to reopen the website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQShareOnline.qml" line="198"/>
        <source>An error occured:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="83"/>
        <source>The background is the area in the back (no surprise there) behind any image that is currently being viewed. By default, PhotoQt is partially transparent with a dark overlay. This is only possible, though, whenever a compositor is available. On some platforms, PhotoQt can fake a transparent background with screenshots taken at startup. Another option is to show a background image (also with a dark overlay) in the background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="98"/>
        <source>real transparency</source>
        <extracomment>How the background of PhotoQt should be</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="108"/>
        <source>fake transparency</source>
        <extracomment>How the background of PhotoQt should be</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="117"/>
        <source>solid background color</source>
        <extracomment>How the background of PhotoQt should be</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="128"/>
        <source>fully transparent background</source>
        <extracomment>How the background of PhotoQt should be</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="146"/>
        <source>Warning: This will make the background fully transparent. This is only recommended if there is a different way to mask the area behind the window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="157"/>
        <source>custom background image</source>
        <extracomment>How the background of PhotoQt should be</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="189"/>
        <source>background image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="207"/>
        <source>Click to select an image</source>
        <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="238"/>
        <source>scale to fit</source>
        <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="245"/>
        <source>scale and crop to fit</source>
        <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="252"/>
        <source>stretch to fit</source>
        <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="259"/>
        <source>center image</source>
        <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="266"/>
        <source>tile image</source>
        <extracomment>If an image is set as background of PhotoQt this is one way it can be shown/scaled</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="295"/>
        <source>overlay with accent color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="301"/>
        <source>overlay with custom color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="334"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="727"/>
        <source>Click to change color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="144"/>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="476"/>
        <source>Click on empty background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="477"/>
        <source>The empty background area is the part of the background that is not covered by any image. A click on that area can trigger certain actions, some depending on where exactly the click occured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="485"/>
        <source>no action</source>
        <extracomment>what to do when the empty background is clicked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="493"/>
        <source>close window</source>
        <extracomment>what to do when the empty background is clicked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="501"/>
        <source>navigate between images</source>
        <extracomment>what to do when the empty background is clicked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="509"/>
        <source>toggle window decoration</source>
        <extracomment>what to do when the empty background is clicked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="145"/>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="560"/>
        <source>Blurring elements behind other elements</source>
        <extracomment>A settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="561"/>
        <source>Whenever an element (e.g., histogram, main menu, etc.) is open, anything behind it can be blurred slightly. This reduces the contrast in the background which improves readability. Note that this requires a slightly higher amount of computations. It also does not work with anything behind PhotoQt that is not part of the window itself.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQBackground.qml" line="569"/>
        <source>Blur elements in the back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQSliderSpinBox.qml" line="149"/>
        <source>Click to edit</source>
        <extracomment>Tooltip, used as in: Click to edit this value</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="83"/>
        <source>The status information refers to the set of information shown in the top left corner of the screen. This typically includes the filename of the currently viewed image and information like the zoom level, rotation angle, etc. The exact set of information and their order can be adjusted as desired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="90"/>
        <source>show status information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="128"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="283"/>
        <source>counter</source>
        <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="130"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="285"/>
        <source>filename</source>
        <extracomment>Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="132"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="287"/>
        <source>filepath</source>
        <extracomment>Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="134"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="289"/>
        <source>resolution</source>
        <extracomment>Please keep short! This is the image resolution.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="136"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="291"/>
        <source>zoom</source>
        <extracomment>Please keep short! This is the current zoom level.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="140"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="295"/>
        <source>filesize</source>
        <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="142"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="297"/>
        <source>color profile</source>
        <extracomment>Please keep short! This is the color profile used for the current image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1556"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="304"/>
        <source>add</source>
        <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="318"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="310"/>
        <source>Font size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="405"/>
        <source>The status info can either be shown at all times, or it can be hidden automatically based on different criteria. It can either be hidden unless the mouse cursor is near the top edge of the screen or until the mouse cursor is moved anywhere. After a specified timeout it will then hide again. In addition to these criteria, it can also be shown shortly whenever the image changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="459"/>
        <source>also show when image changes</source>
        <extracomment>Refers to the status information&apos;s auto-hide feature, this is an additional case it can be shown</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="533"/>
        <source>Position</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="535"/>
        <source>The status info is typically shown along the top left corner of the window. If preferred, it is also possible to show it centered along the top edge or in the top right corner.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="582"/>
        <source>By default it is possible to drag the status info around as desired. However, it is also possible to use the status info for managing the window itself. When enabled, dragging the status info will drag the window around, and double clicking the status info will toggle the maximized status of the window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="588"/>
        <source>manage window through status info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="355"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="303"/>
        <source>Visibility</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="411"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="412"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="68"/>
        <source>Motion/Live photos</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="413"/>
        <location filename="../qml/settingsmanager/settings/filetypes/PQAdvanced.qml" line="163"/>
        <source>Photo spheres</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="425"/>
        <source>Keyboard &amp; Mouse</source>
        <extracomment>A settings category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="428"/>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="429"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="251"/>
        <source>Shortcuts</source>
        <extracomment>A settings subcategory
----------
Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="456"/>
        <source>Session</source>
        <extracomment>A settings subcategory</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="457"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="68"/>
        <source>Single instance</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="70"/>
        <source>PhotoQt can either run in single-instance mode or allow multiple instances to run at the same time. The former has the advantage that it is possible to interact with a running instance of PhotoQt through the command line (in fact, this is a requirement for that to work). The latter allows, for example, for the comparison of multiple images side by side.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="77"/>
        <source>run a single instance only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="84"/>
        <source>allow multiple instances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="458"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="124"/>
        <source>Reopen last image</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="126"/>
        <source>When PhotoQt is started normally, by default an empty window is shown with the prompt to open an image from the file dialog. Alternatively it is also possible to reopen the image that was last loaded in the previous session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="133"/>
        <source>start with blank session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="140"/>
        <source>reopen last used image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="459"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="180"/>
        <source>Remember changes</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="182"/>
        <source>Once an image has been loaded it can be manipulated freely by zooming, rotating, or mirroring the image. Once another image is loaded any such changes are forgotten. If preferred, it is possible for PhotoQt to remember any such manipulations per session. Note that once PhotoQt is closed these changes will be forgotten in any case.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="182"/>
        <source>In addition to on an per-image basis, PhotoQt can also keep the same changes across different images. If enabled and possible, the next image is loaded with the same scaling, rotation, and mirroring as the image before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="191"/>
        <source>forget changes when other image loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="199"/>
        <source>remember changes per session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="209"/>
        <source>preserve across images:</source>
        <extracomment>this refers to preserving any selection of zoom/rotation/mirror across different images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="221"/>
        <source>Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="227"/>
        <source>Mirror</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="460"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="287"/>
        <source>Tray Icon</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="289"/>
        <source>PhotoQt can show a small icon in the system tray. The tray icon provides additional ways to control and interact with the application. It is also possible to hide PhotoQt to the system tray instead of closing. By default a colored version of the tray icon is used, but it is also possible to use a monochrome version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="296"/>
        <source>Show tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="315"/>
        <source>monochrome icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="323"/>
        <source>hide to tray icon instead of closing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="461"/>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="380"/>
        <source>Reset when hiding</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="382"/>
        <source>When hiding PhotoQt in the system tray, it is possible to reset PhotoQt to its initial state, thus freeing most of the memory tied up by caching. Note that this will also unload any loaded folder and image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQSession.qml" line="388"/>
        <source>reset session when hiding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="434"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="79"/>
        <source>Move image with mouse</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="81"/>
        <source>PhotoQt can use both the left button of the mouse and the mouse wheel to move the image around. In that case, however, these actions are not available for shortcuts anymore, except when combined with one or more modifier buttons (Alt, Ctrl, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="88"/>
        <source>move image with left button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="95"/>
        <source>move image with mouse wheel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="197"/>
        <source>very sensitive</source>
        <extracomment>used as in: very sensitive mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="207"/>
        <source>not sensitive</source>
        <extracomment>used as in: not at all sensitive mouse wheel</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="253"/>
        <source>hide cursor after timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="308"/>
        <source>Escape key handling</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="310"/>
        <source>The Escape key can be used to cancel special actions or modes instead of any configured shortcut action. Here you can enable or disable any one of them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="317"/>
        <source>leave document viewer if inside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="323"/>
        <source>leave archive viewer if inside</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="329"/>
        <source>hide barcodes if any visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="335"/>
        <source>remove filter if any set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="341"/>
        <source>leave photo sphere if any entered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="435"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="136"/>
        <source>Double click</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="138"/>
        <source>A double click is defined as two clicks in quick succession. This means that PhotoQt will have to wait a certain amount of time to see if there is a second click before acting on a single click. Thus, the threshold (specified in milliseconds) for detecting double clicks should be as small as possible while still allowing for reliable detection of double clicks. Setting this value to zero disables double clicks and treats them as two distinct single clicks.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="436"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="187"/>
        <source>Mouse wheel</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="189"/>
        <source>Depending on any particular hardware, the mouse wheel moves either a set amount each time it is moved, or relative to how long/fast it is moved. The sensitivity allows to account for very sensitive hardware to decrease the likelihood of accidental/multiple triggers caused by wheel movement.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="437"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="244"/>
        <source>Hide mouse cursor</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQBehavior.qml" line="246"/>
        <source>Whenever an image is viewed and mouse cursor rests on the image it is possible to hide the mouse cursor after a set timeout. This way the cursor does not get in the way of actually viewing an image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="352"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="73"/>
        <source>Spacing</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="75"/>
        <source>PhotoQt preloads thumbnails for all files in the current folder and lines them up side by side. In between each thumbnail image it is possible to add a little bit of blank space to better separate the individual images.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="353"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="125"/>
        <source>Highlight</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="127"/>
        <source>The thumbnail corresponding to the currently loaded image is highlighted so that it is easy to spot. The same highlight effect is used when hovering over a thumbnail image. The different effects can be combined as desired.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="135"/>
        <source>invert background color</source>
        <extracomment>effect for highlighting active thumbnail</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="143"/>
        <source>invert label color</source>
        <extracomment>effect for highlighting active thumbnail</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="151"/>
        <source>line below</source>
        <extracomment>effect for highlighting active thumbnail</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="159"/>
        <source>magnify</source>
        <extracomment>effect for highlighting active thumbnail</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="171"/>
        <source>lift up</source>
        <extracomment>effect for highlighting active thumbnail</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="751"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="313"/>
        <source>hide when not needed</source>
        <extracomment>used as in: hide thumbnail bar when not needed</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="354"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="259"/>
        <source>Center on active</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="261"/>
        <source>When switching between images PhotoQt always makes sure that the thumbnail corresponding to the currently viewed image is visible somewhere along the thumbnail bar. Additionally it is possible to tell PhotoQt to not only keep it visible but also keep it in the center of the edge.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="267"/>
        <source>keep active thumbnail in center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="305"/>
        <source>The visibility of the thumbnail bar can be set depending on personal choice. The bar can either always be kept visible, it can be hidden unless the mouse cursor is close to the respective screen edge, or it can be kept visible unless the main image has been zoomed in.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="681"/>
        <source>fit thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="695"/>
        <source>scale and crop thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="763"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="321"/>
        <source>always keep visible</source>
        <extracomment>used as in: always keep thumbnail bar visible</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="775"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQAllThumbnails.qml" line="329"/>
        <source>hide when zoomed in</source>
        <extracomment>used as in: hide thumbnail bar when zoomed in</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="790"/>
        <source>show filename labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="336"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="76"/>
        <source>Size</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="78"/>
        <source>The thumbnails are typically hidden behind one of the screen edges. Which screen edge can be specified in the interface settings. The size of the thumbnails refers to the maximum size of each individual thumbnail image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="337"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="911"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="127"/>
        <source>Scale and crop</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="129"/>
        <source>The thumbnail for an image can either be scaled to fit fully inside the maximum size specified above, or it can be scaled and cropped such that it takes up all available space. In the latter case some parts of a thumbnail image might be cut off. In addition, thumbnails that are smaller than the size specified above can be kept at their original size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="130"/>
        <source>A third option is to scale all thumbnails to the height of the bar and vary their width. Note that this requires all thumbnails to be preloaded at the start potentially causing short stutters in the interface. Such a listing of images at various widths can also be a little more difficult to scroll through.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="137"/>
        <source>fit thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="144"/>
        <source>scale and crop thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="709"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="151"/>
        <source>same height, varying width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="736"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="163"/>
        <source>keep small thumbnails small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="273"/>
        <source>On top of each thumbnail image PhotoQt can put a small text label with the filename. The font size of the filename is freely adjustable. If a filename is too long for the available space only the beginning and end of the filename will be visible. Additionally, the label of thumbnails that are neither loaded nor hovered can be shown with less opacity.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="321"/>
        <source>decrease opacity for inactive thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="338"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="214"/>
        <source>Icons only</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="216"/>
        <source>Instead of loading actual thumbnail images in the background, PhotoQt can instead simply show the respective icon for the filetype. This requires much fewer resources and time but is not as user friendly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="224"/>
        <source>use actual thumbnail images</source>
        <extracomment>The word actual is used with the same meaning as: real</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="231"/>
        <source>use filetype icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="339"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="271"/>
        <source>Label</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="280"/>
        <source>show filename label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="340"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="269"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="369"/>
        <source>Tooltip</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="371"/>
        <source>PhotoQt can show additional information about an image in the form of a tooltip that is shown when the mouse cursor hovers above a thumbnail. The displayed information includes the full file name, file size, and file type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="798"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQImageSetThumb.qml" line="377"/>
        <source>show tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="88"/>
        <source>PhotoQt can cache thumbnails so that each subsequent time they can be generated near instantaneously. PhotoQt implements the standard for thumbnails defined by freedesktop.org. On Windows it can also load (but not write) existing thumbnails from the thumbnail cache built into Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="94"/>
        <source>enable cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="365"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="182"/>
        <source>Exclude folders</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="73"/>
        <source>To disable thumbnail altogether remove it from all screen edges in the Interface category.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="109"/>
        <source>use default cache directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="125"/>
        <source>Click to select custom base directory for thumbnail cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="184"/>
        <source>When an image is loaded PhotoQt preloads thumbnails for all images found in the current folder. Some cloud providers do not fully sync their files unless accessed. To avoid unnecessarily downloading large amount of files, it is possible to exclude specific directories from any sort of caching and preloading. Note that for files in these folders you will still see thumbnails consisting of filetype icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="192"/>
        <source>Cloud providers to exclude from caching:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="229"/>
        <source>Do not cache these folders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="236"/>
        <source>One folder per line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="246"/>
        <source>Add folder</source>
        <extracomment>Written on a button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="366"/>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="354"/>
        <source>How many threads</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/thumbnails/PQManage.qml" line="356"/>
        <source>In order to speed up loading all the thumbnails in a folder PhotoQt uses multiple threads simultaneously. On more powerful systems, a larger number of threads can result in much faster loading of all the thumbnails of a folder. Too many threads, however, might make a system feel slow for a short time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="475"/>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="204"/>
        <source>Export/Import configuration</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="206"/>
        <source>Here you can create a backup of the configuration for backup or for moving it to another install of PhotoQt. You can import a local backup below. After importing a backup file PhotoQt will automatically close as it will need to be restarted for the changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="212"/>
        <source>export configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="220"/>
        <source>import configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="224"/>
        <source>Restart required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="225"/>
        <source>PhotoQt will now quit as it needs to be restarted for the changes to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="474"/>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="70"/>
        <source>Reset settings and shortcuts</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="72"/>
        <source>Here the various configurations of PhotoQt can be reset to their defaults. Once you select one of the below categories you have a total of 5 seconds to cancel the action. After the 5 seconds are up the respective defaults will be set. This cannot be undone.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="78"/>
        <source>reset settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="89"/>
        <source>reset shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="100"/>
        <source>reset enabled file formats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="132"/>
        <source>You can still cancel this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="137"/>
        <source>%1 seconds remaining!</source>
        <extracomment>Please don&apos;t forget the placeholder. It will be replaced by the number of seconds.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="228"/>
        <source>Import failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/manage/PQConfiguration.qml" line="229"/>
        <source>The configuration could not be imported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="431"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="544"/>
        <source>music volume during videos with audio:</source>
        <extracomment>some options as to what will happen with the music volume while videos are playing
----------
some options as to what will happen with the slideshow music volume while videos are playing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="436"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="550"/>
        <source>mute</source>
        <extracomment>one option as to what will happen with the slideshow music volume while videos are playing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="437"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="552"/>
        <source>lower</source>
        <extracomment>one option as to what will happen with the slideshow music volume while videos are playing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="438"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="554"/>
        <source>leave unchanged</source>
        <extracomment>one option as to what will happen with the slideshow music volume while videos are playing</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="465"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="583"/>
        <source>No music files selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="543"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="647"/>
        <source>Move file up one position</source>
        <extracomment>This relates to the list of music files for slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="559"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="665"/>
        <source>Move file down one position</source>
        <extracomment>This relates to the list of music files for slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="573"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="681"/>
        <source>Delete this file from the list</source>
        <extracomment>This relates to the list of music files for slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="589"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="700"/>
        <source>Add music files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="605"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="716"/>
        <source>shuffle order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="524"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="64"/>
        <source>file name</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="525"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="72"/>
        <source>dimensions</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="526"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="70"/>
        <source>image #/#</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="527"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="68"/>
        <source>file size</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="528"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="66"/>
        <source>file type</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="529"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="93"/>
        <source>make</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="530"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="95"/>
        <source>model</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="531"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="99"/>
        <source>software</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="532"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="101"/>
        <source>time photo was taken</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="533"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="76"/>
        <source>exposure time</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="534"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="78"/>
        <source>flash</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="536"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="97"/>
        <source>scene type</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="537"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="80"/>
        <source>focal length</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="538"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="82"/>
        <source>f-number</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="539"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="89"/>
        <source>light source</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="540"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="87"/>
        <source>keywords</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="541"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="91"/>
        <source>location</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="542"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="74"/>
        <source>copyright</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="543"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="84"/>
        <source>GPS position</source>
        <extracomment>Part of the meta information about the current image.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="622"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="508"/>
        <source>hide behind screen edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="632"/>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="516"/>
        <source>use floating element</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQMetaData.qml" line="685"/>
        <location filename="../qml/ongoing/PQQuickActions.qml" line="469"/>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="679"/>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="807"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="500"/>
        <source>Manage in settings manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="585"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="401"/>
        <source>show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="595"/>
        <source>manage window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="603"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="453"/>
        <source>visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="608"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="458"/>
        <source>always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="621"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="471"/>
        <source>cursor move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="634"/>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="484"/>
        <source>cursor near top edge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="649"/>
        <source>position</source>
        <extracomment>The position here refers to the position of the statusinfo, where along the top edge of the window it should be aligned along</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="652"/>
        <source>top left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="660"/>
        <source>top center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="668"/>
        <source>top right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="414"/>
        <source>duplicate buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="424"/>
        <source>navigation icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="427"/>
        <source>show icons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQWindowButtons.qml" line="438"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="402"/>
        <source>only in fullscreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQCategory.qml" line="329"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="112"/>
        <source>Filter</source>
        <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="77"/>
        <source>images</source>
        <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="79"/>
        <source>compressed files</source>
        <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="81"/>
        <source>documents</source>
        <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="83"/>
        <source>videos</source>
        <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="90"/>
        <source>Enable</source>
        <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="97"/>
        <source>Disable</source>
        <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="123"/>
        <source>Enable everything</source>
        <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="135"/>
        <source>Currently there are %1 file formats enabled</source>
        <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="164"/>
        <source>Search by description or file ending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="182"/>
        <source>Search by image library or category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/filetypes/PQFileTypes.qml" line="292"/>
        <source>File endings:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="76"/>
        <source>When loading an image PhotoQt loads all images in the folder as thumbnails for easy navigation. When PhotoQt reaches the end of the list of files, it can either stop right there or loop back to the other end of the list and keep going.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="83"/>
        <source>Loop around</source>
        <extracomment>When reaching the end of the images in the folder whether to loop back around to the beginning or not</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="121"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="77"/>
        <source>Images in a folder can be sorted in different ways. Once a folder is loaded it is possible to further sort a folder in several advanced ways using the menu option for sorting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="131"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="85"/>
        <source>Sort by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="136"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="90"/>
        <source>natural name</source>
        <extracomment>A criteria for sorting images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="138"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="92"/>
        <source>name</source>
        <extracomment>A criteria for sorting images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="140"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="94"/>
        <source>time</source>
        <extracomment>A criteria for sorting images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="142"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="96"/>
        <source>size</source>
        <extracomment>A criteria for sorting images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="144"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="98"/>
        <source>type</source>
        <extracomment>A criteria for sorting images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="157"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="111"/>
        <source>ascending order</source>
        <extracomment>Sort images in ascending order</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="163"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="117"/>
        <source>descending order</source>
        <extracomment>Sort images in descending order</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="225"/>
        <source>When switching between images PhotoQt can add an animation to smoothes such a transition. There are a whole bunch of transitions to choose from, and also an option for PhotoQt to choose one at random each time. Additionally, the speed of the chosen animation can be chosen from very slow to very fast.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="232"/>
        <source>animate switching between images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="255"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="118"/>
        <source>Animation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="260"/>
        <source>opacity</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="262"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="127"/>
        <source>along x-axis</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="264"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="129"/>
        <source>along y-axis</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="266"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="138"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="293"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="131"/>
        <source>rotation</source>
        <extracomment>This is referring to an in/out animation of images
----------
Please keep short! This is the rotation of the current image</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="268"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="133"/>
        <source>explosion</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="270"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="135"/>
        <source>implosion</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="272"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="137"/>
        <source>choose one at random</source>
        <extracomment>This is referring to an in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="291"/>
        <source>speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="301"/>
        <source>(higher value = slower)</source>
        <extracomment>The value is a numerical value expressing the speed of animating between images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="367"/>
        <source>The number of images in both directions (previous and next) that should be preloaded in the background. Images are not preloaded until the main image has been displayed. This improves navigating through all images in the folder, but the tradeoff is an increased memory consumption. It is recommended to keep this at a low number.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="392"/>
        <source>only current image will be loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="394"/>
        <source>preload 1 image in both directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQFolder.qml" line="395"/>
        <source>preload %1 images in both directions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="78"/>
        <source>PhotoQt allows for a great deal of flexibility in viewing images at the perfect size. Additionally it allows for control of how fast the zoom happens (both in relative and absolute terms), and if there is a minimum/maximum zoom level at which it should always stop no matter what. Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="87"/>
        <source>zoom speed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="96"/>
        <source>relative zoom speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="102"/>
        <source>absolute zoom speed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="117"/>
        <source>minimum zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="142"/>
        <source>maximum zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="173"/>
        <source>Zoom to/from:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="179"/>
        <source>mouse position</source>
        <extracomment>refers to where to zoom to/from</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="186"/>
        <source>image center</source>
        <extracomment>refers to where to zoom to/from</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="270"/>
        <source>The minimap is a small version of the image that is shown in the lower right corner whenever the image has been zoomed in. It shows the currently visible section of the image and allows to navigate to other parts of the image by clicking at a location or by dragging the highlighted rectangle.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="276"/>
        <source>Show minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="289"/>
        <source>small minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="290"/>
        <source>normal minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="291"/>
        <source>large minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="292"/>
        <source>very large minimap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="339"/>
        <source>Images can be manipulated inside PhotoQt in a variety of ways, including their zoom and rotation. Another property that can be manipulated is the mirroring (or flipping) of images both vertically and horizontally. By default, PhotoQt animates this process, but this behavior can be disabled here. In that case the mirror/flip happens instantaneously.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="345"/>
        <source>Animate mirror/flip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="383"/>
        <source>Switching between images can be done in various ways. It is possible to do so through the shortcuts, through the main menu, or through floating navigation buttons. These floating buttons were added especially with touch screens in mind, as it allows easier navigation without having to use neither the keyboard nor the mouse. In addition to buttons for navigation it also includes a button to hide and show the main menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQInteraction.qml" line="389"/>
        <source>show floating navigation buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="134"/>
        <source>Whenever an image is loaded PhotoQt tries to find as much metadata about the image as it can. The found information is then displayed in the metadata element that can be accesses either through one of the screen edges or as floating element. Since not all information might be wanted by everyone, individual information labels can be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="150"/>
        <source>Filter labels</source>
        <extracomment>placeholder text in a text edit</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="387"/>
        <source>When an image is taken with the camera turned on its side, some cameras store that rotation in the metadata. PhotoQt can use that information to display an image the way it was meant to be viewed. Disabling this will load all photos without any rotation applied by default.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="393"/>
        <source>Apply default rotation automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="431"/>
        <source>Some cameras store the location of where the image was taken in the metadata of its images. PhotoQt can use that information in multiple ways. It can show a floating embedded map with a pin on that location, and it can show the GPS coordinates in the metadata element. In the latter case, a click on the GPS coordinates will open the location in an online map service, the choice of which can be set here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="501"/>
        <source>The metadata element can be show in two different ways. It can either be shown hidden behind one of the screen edges and shown when the cursor is close to said edge. Or it can be shown as floating element that can be triggered by shortcut and stays visible until manually hidden.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="559"/>
        <source>PhotoQt can read face tags stored in its metadata. It offers a great deal of flexibility in how and when the face tags are shown. It is also possible to remove and add face tags using the face tagger interface (accessible through the context menu or by shortcut).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="566"/>
        <source>show face tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="586"/>
        <source>always show all</source>
        <extracomment>used as in: always show all face tags</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="594"/>
        <source>show one on hover</source>
        <extracomment>used as in: show one face tag on hover</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="602"/>
        <source>show all on hover</source>
        <extracomment>used as in: show one face tag on hover</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="661"/>
        <source>It is possible to adjust the border shown around tagged faces and the font size used for the displayed name. For the border, not only the width but also the color can be specified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="669"/>
        <source>font size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="678"/>
        <source>show border around face tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="701"/>
        <source>border width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/imageview/PQMetadata.qml" line="713"/>
        <source>color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="93"/>
        <source>The context menu contains actions that can be performed related to the currently viewed image. By default is it shown when doing a right click on the background, although it is possible to change that in the shortcuts category. In addition to pre-defined image functions it is also possible to add custom entries to that menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="104"/>
        <source>No custom entries exists yet</source>
        <extracomment>The custom entries here are the custom entries in the context menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="135"/>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="194"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="317"/>
        <source>Select</source>
        <extracomment>written on button for selecting a file from the file dialog
----------
written on button in file picker to select an existing executable file</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="158"/>
        <source>entry name</source>
        <extracomment>The entry here refers to the text that is shown in the context menu for a custom entry</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="175"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="301"/>
        <source>executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="190"/>
        <source>Select executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="230"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="351"/>
        <source>additional flags</source>
        <extracomment>The flags here are additional parameters that can be passed on to an executable
----------
the flags here are parameters specified on the command line</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="246"/>
        <source>quit</source>
        <extracomment>Quit PhotoQt after executing custom context menu entry. Please keep as short as possible!!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="277"/>
        <source>Delete entry</source>
        <extracomment>The entry here is a custom entry in the context menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="293"/>
        <source>Add new entry</source>
        <extracomment>The entry here is a custom entry in the context menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="312"/>
        <source>Add system applications</source>
        <extracomment>The system applications here refers to any image related applications that can be found automatically on your system</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="358"/>
        <source>The custom context menu entries can also be duplicated in the main menu. If enabled, the entries set above will be accesible in both places.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQContextMenuSet.qml" line="365"/>
        <source>Duplicate in main menu</source>
        <extracomment>Refers to duplicating the custom context menu entries in the main menu</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="73"/>
        <source>No action</source>
        <extracomment>Used as descriptor for a screen edge action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="77"/>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="86"/>
        <source>Main menu</source>
        <extracomment>Used as descriptor for a screen edge action
----------
Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="102"/>
        <source>Moving the mouse cursor to the edges of the application window can trigger the visibility of some things, like the main menu, thumbnails, or metadata. Here you can choose what is triggered by which window edge. Note that if the main menu is completely disabled, then the settings manager can still be accessed by shortcut or through the context menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="134"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="167"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="196"/>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="228"/>
        <source>Click to change action</source>
        <extracomment>The action here is a screen edge action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQEdges.qml" line="291"/>
        <source>The edge actions defined above are triggered whenever the mouse cursor gets close to the screen edge. The sensitivity determines how close to the edge the mouse cursor needs to be for this to happen. A value that is too sensitive might cause the edge action to sometimes be triggered accidentally.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="73"/>
        <source>PhotoQt has been translated into a number of different languages. Not all of the languages have a complete translation yet, and new translators are always needed. If you are willing and able to help, that would be greatly appreciated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="74"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="150"/>
        <source>Thank you to all who volunteered their time to help translate PhotoQt into other languages!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="75"/>
        <source>If you want to help with the translations, either by translating or by reviewing existing translations, head over to the translation page on Crowdin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="224"/>
        <source>There are two main states that the application window can be in. It can either be in fullscreen mode or in window mode. In fullscreen mode, PhotoQt will act more like a floating layer that allows you to quickly look at images. In window mode, PhotoQt can be used in combination with other applications. When in window mode, it can also be set to always be above any other windows, and to remember the window geometry in between sessions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="235"/>
        <source>fullscreen mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="241"/>
        <source>window mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="262"/>
        <source>keep above other windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="269"/>
        <source>remember its geometry </source>
        <extracomment>remember the geometry of PhotoQts window between sessions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="275"/>
        <source>enable window decoration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="347"/>
        <source>PhotoQt can show some integrated window buttons for basic window managements both when shown in fullscreen and when in window mode. In window mode with window decoration enabled it can either hide or show buttons from its integrated set that are duplicates of buttons in the window decoration. For help with navigating through a folder, small left/right arrows for navigation and a menu button can also be added next to the window buttons. There are also various visibility tweaks that can be adjusted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="357"/>
        <source>show integrated window buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="382"/>
        <source>duplicate buttons from window decoration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="390"/>
        <source>add navigation buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="417"/>
        <source>left of window buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="422"/>
        <source>right of window buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="436"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="451"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="412"/>
        <source>keep always visible</source>
        <extracomment>visibility status of the window buttons
----------
visibility status of the status information</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="459"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="420"/>
        <source>only show with any cursor move</source>
        <extracomment>visibility status of the window buttons
----------
visibility status of the status information</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="467"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="428"/>
        <source>only show when cursor near top edge</source>
        <extracomment>visibility status of the window buttons
----------
visibility status of the status information</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="476"/>
        <location filename="../qml/settingsmanager/settings/interface/PQStatusInfoSet.qml" line="437"/>
        <source>hide again after timeout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="578"/>
        <source>Here an accent color of PhotoQt can be selected, with the whole interface colored with shades of it. After selecting a new color it is recommended to first test the color using the provided button to make sure that the interface is readable with the new color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="588"/>
        <source>custom color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="645"/>
        <source>Test color for %1 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="688"/>
        <source>use accent color for background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="694"/>
        <source>use custom color for background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="823"/>
        <source>All text in PhotoQt is shown with one of two weights, either as regular text or in bold face. Here the actual weight used can be adjusted for the two types. The default weight for normal text is 400 and for bold text is 700.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="830"/>
        <source>thin</source>
        <extracomment>This refers to a type of font weight (thin is the lightest weight)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="832"/>
        <source>very light</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="834"/>
        <source>light</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="836"/>
        <source>normal</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="838"/>
        <source>medium</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="840"/>
        <source>medium bold</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="842"/>
        <source>bold</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="844"/>
        <source>extra bold</source>
        <extracomment>This refers to a type of font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="846"/>
        <source>black</source>
        <extracomment>This refers to a type of font weight (black is the darkest, most bold weight)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="856"/>
        <source>normal font weight:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="870"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="898"/>
        <source>current weight:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="884"/>
        <source>bold font weight:</source>
        <extracomment>The weight here refers to the font weight</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="937"/>
        <source>For certain actions a notification is shown. On Linux this notification can be shown as native notification. Alternatively it can also be shown integrated into the main interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="971"/>
        <source>top</source>
        <extracomment>Vertical position of the integrated notification popup. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="979"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1005"/>
        <source>center</source>
        <extracomment>Vertical position of the integrated notification popup. Please keep short!
----------
Horizontal position of the integrated notification popup. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="987"/>
        <source>bottom</source>
        <extracomment>Vertical position of the integrated notification popup. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="999"/>
        <source>left</source>
        <extracomment>Horizontal position of the integrated notification popup. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1011"/>
        <source>right</source>
        <extracomment>Horizontal position of the integrated notification popup. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1031"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1051"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1071"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1094"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1114"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1134"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1157"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1177"/>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1197"/>
        <source>Show notification at this position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1213"/>
        <source>Distance from edge:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1222"/>
        <source>try to show native notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1279"/>
        <source>Quick Actions</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1281"/>
        <source>The quick actions are some actions that can be performed with a currently viewed image. They allow for quickly performing an action with the mouse with a single click.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQInterface.qml" line="1290"/>
        <source>show quick actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="82"/>
        <source>Map explorer</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="84"/>
        <source>Settings manager</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="90"/>
        <source>Histogram</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="92"/>
        <source>Map (current image)</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="94"/>
        <source>Scale</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="96"/>
        <source>Slideshow setup</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="98"/>
        <source>Slideshow controls</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="100"/>
        <source>Rename file</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="102"/>
        <source>Delete file</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="104"/>
        <source>Export file</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="106"/>
        <source>About</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="108"/>
        <source>Imgur</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="110"/>
        <source>Wallpaper</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="114"/>
        <source>Advanced image sort</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="116"/>
        <source>Streaming (Chromecast)</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="120"/>
        <source>Crop</source>
        <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="154"/>
        <source>Almost all of the elements for displaying information or performing actions can either be shown integrated into the main window or shown popped out in their own window. Most of them can also be popped out/in through a small button at the top left corner of each elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="170"/>
        <source>Filter popouts</source>
        <extracomment>placeholder text in a text edit</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="396"/>
        <source>Non-modal popouts</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="398"/>
        <source>All popouts by default are modal windows. That means that they block the main interface until they are closed again. Some popouts can be switched to a non-modal behavior, allowing them to stay open while using the main interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="398"/>
        <source>Please note: If a popout is set to be non-modal then it will not be able to receive any shortcut commands anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="405"/>
        <source>make file dialog non-modal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="413"/>
        <source>make map explorer non-modal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="421"/>
        <source>make settings manager non-modal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="469"/>
        <source>Some elements might not be as usable or function well when the window is too small. Thus it is possible to force such elements to be popped out automatically whenever the application window is too small.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/interface/PQPopout.qml" line="476"/>
        <source>pop out when application window is small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="67"/>
        <source>These settings can also be adjusted from within the file dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="171"/>
        <source>The files can be shown either as icons with an emphasis on the thumbnails, or as list with an emphasis on getting a clear overview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="178"/>
        <source>icon view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="185"/>
        <source>list view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="225"/>
        <source>Hidden files</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="227"/>
        <source>Hidden files and folders are by default not included in the list of files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="233"/>
        <source>Show hidden files/folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="271"/>
        <source>When moving the mouse cursor over an entry, a tooltip with a larger preview and more information about the file or folder can be shown.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="277"/>
        <source>Show tooltip with details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="314"/>
        <source>Last location</source>
        <extracomment>Settings title, location here is a folder path</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="316"/>
        <source>By default the file dialog starts out in your home folder at start. Enabling this setting makes the file dialog reopen at the same location where it ended in the last session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="322"/>
        <source>Remember</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="359"/>
        <source>Single clicks</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="361"/>
        <source>By default the behavior of single clicks follows the standard behavior on Linux, where a single click opens a file or folder. Enabling this setting results in single clicks only selecting files and folders with double clicks required to actually open them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="367"/>
        <source>Open with single click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="374"/>
        <source>Select with single click, open with double click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="414"/>
        <source>Selection</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="416"/>
        <source>Usually, once a folder is navigated away from any selection is lost. However, it is possible to remember the file/folder selection for each folder and have it recalled next time the folder is loaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="422"/>
        <source>Remember selection for each folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="461"/>
        <source>In the left column there are two sections that can be shown. The bookmarks are a combination of some standard locations on any computer and a customizable list of your own bookmarks. The devices are a list of storage devices found on your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="467"/>
        <source>Show bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="473"/>
        <source>Show devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="481"/>
        <source>Include temporary devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="528"/>
        <source>There are two different drag-and-drop actions that exist in the file dialog. 1) It is possible to drag folders from either the list or icon view (or both) and drop them on the bookmarks. And 2) it is possible to reorder the bookmarks through drag-and-drop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="593"/>
        <source>For all files PhotoQt can either show an icon corresponding to its file type or a small thumbnail preview. The thumbnail can either be shown fitted into the available space or cropped to fill out the full space.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="599"/>
        <source>Show thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="617"/>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="778"/>
        <source>Scale and crop thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="661"/>
        <source>The empty space between the different thumbnails.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="712"/>
        <source>When hovering over a folder PhotoQt can give a preview of that folder by iterating through thumbnails of its content. Additionally, the timeout before changing the thumbnail and whether to loop around can be adjusted. Enabling the auto load setting preloads the first thumbnail when the parent folder is opened.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="718"/>
        <source>Enable folder thumbnails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="740"/>
        <source>Loop through content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="755"/>
        <source>Timeout:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="771"/>
        <source>Auto-load first thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="841"/>
        <source>The preview refers to the larger preview of a file shown behind the list of all files and folders. Various properties of that preview can be adjusted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="848"/>
        <source>Show preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="878"/>
        <source>color intensity:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="890"/>
        <source>Blur the preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="897"/>
        <source>Mute its colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQFileDialog.qml" line="904"/>
        <source>Higher resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="72"/>
        <source>These settings can also be adjusted when setting up a slideshow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="87"/>
        <source>The animation for switching images can be customized for slideshows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="94"/>
        <source>Enable animations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="256"/>
        <source>This determines how long PhotoQt waits before switching to the next image in the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="264"/>
        <source>interval:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="307"/>
        <source>When the end of the image list has been reached PhotoQt can go to the first image and keep going.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="313"/>
        <source>loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="352"/>
        <source>The images can either be shown in their normal or a randomly shuffled order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="358"/>
        <source>shuffle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="530"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="395"/>
        <source>Status info and window buttons</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="397"/>
        <source>The status info and the window buttons can either remain visible or be hidden during slideshows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="403"/>
        <source>hide status info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="410"/>
        <source>hide window buttons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="453"/>
        <source>When starting a slideshow PhotoQt can also include images in subfolders.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="459"/>
        <source>include subfolders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="496"/>
        <source>Music files</source>
        <extracomment>Settings title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="498"/>
        <source>PhotoQt can play some background music while a slideshow is running. You can select an individual file or multiple files. PhotoQt will restart from the beginning once the end is reached. During videos the volume of the music can optionally be reduced.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="510"/>
        <source>enable music</source>
        <extracomment>Enable music to be played during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="71"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="316"/>
        <source>Viewing images</source>
        <extracomment>This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="73"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="317"/>
        <source>Current image</source>
        <extracomment>This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="75"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="318"/>
        <source>Current folder</source>
        <extracomment>This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="81"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="321"/>
        <source>External</source>
        <extracomment>This is a shortcut category</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="85"/>
        <source>These actions affect the behavior of PhotoQt when viewing images. They include actions for navigating between images and manipulating the current image (zoom, flip, rotation) amongst others.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="86"/>
        <source>These actions are certain things that can be done with the currently viewed image. They typically do not affect any of the other images.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="87"/>
        <source>These actions affect the currently loaded folder as a whole and not just single images.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="88"/>
        <source>These actions affect the status and behavior of various interface elements regardless of the status of any possibly loaded image.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="89"/>
        <source>These actions do not fit into any other category.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="90"/>
        <source>Here any external executable can be set as shortcut action. The button with the three dots can be used to select an executable with a file dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="114"/>
        <source>Shortcut actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="289"/>
        <source>Select an executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="307"/>
        <source>Click here to select an executable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="341"/>
        <source>Additional flags and options to pass on:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="356"/>
        <source>Note that relative file paths are not supported, however, you can use the following placeholders:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="357"/>
        <source>filename including path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="358"/>
        <source>filename without path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="359"/>
        <source>directory containing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="360"/>
        <source>If you type out a path, make sure to escape spaces accordingly by prepending a backslash:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="372"/>
        <source>quit after calling executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewAction.qml" line="382"/>
        <source>Save external action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
        <source>Add New Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="125"/>
        <source>Set new shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="147"/>
        <source>Perform a mouse gesture here or press any key combo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="179"/>
        <source>The left button is used for moving the main image around.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQNewShortcut.qml" line="180"/>
        <source>It can be used as part of a shortcut only when combined with modifier buttons (Alt, Ctrl, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="59"/>
        <source>Next image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="61"/>
        <source>Previous image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="63"/>
        <source>Go to first image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="65"/>
        <source>Go to last image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="67"/>
        <source>Zoom In</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="69"/>
        <source>Zoom Out</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="71"/>
        <source>Zoom to Actual Size</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="73"/>
        <source>Reset Zoom</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="75"/>
        <source>Rotate Right</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="77"/>
        <source>Rotate Left</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="79"/>
        <source>Reset Rotation</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="81"/>
        <source>Mirror Horizontally</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="83"/>
        <source>Mirror Vertically</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="85"/>
        <source>Reset Mirror</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="87"/>
        <source>Load a random image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="89"/>
        <source>Hide/Show face tags (stored in metadata)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="91"/>
        <source>Toggle: Fit in window</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="93"/>
        <source>Toggle: Show always actual size by default</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="95"/>
        <source>Stream content to Chromecast device</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="97"/>
        <source>Move view left</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="99"/>
        <source>Move view right</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="101"/>
        <source>Move view up</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="103"/>
        <source>Move view down</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="105"/>
        <source>Go to left edge of image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="107"/>
        <source>Go to right edge of image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="109"/>
        <source>Go to top edge of image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="111"/>
        <source>Go to bottom edge of image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="117"/>
        <source>Show Histogram</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="119"/>
        <source>Show Image on Map</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="121"/>
        <source>Enter viewer mode</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="123"/>
        <source>Scale Image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="125"/>
        <source>Rename File</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="127"/>
        <source>Delete File</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="129"/>
        <source>Delete File permanently (without confirmation)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="131"/>
        <source>Move file to trash (without confirmation)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="133"/>
        <source>Restore file from trash</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="135"/>
        <source>Copy File to a New Location</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="137"/>
        <source>Move File to a New Location</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="139"/>
        <source>Copy Image to Clipboard</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="141"/>
        <source>Save image in another format</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="143"/>
        <source>Print current photo</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="145"/>
        <source>Set as Wallpaper</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="147"/>
        <source>Upload to imgur.com (anonymously)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="149"/>
        <source>Upload to imgur.com user account</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="151"/>
        <source>Play/Pause animation/video</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="153"/>
        <source>Go ahead 5 seconds in video</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="155"/>
        <source>Go back 5 seconds in video</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="157"/>
        <source>Start tagging faces</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="159"/>
        <source>Enter photo sphere</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="161"/>
        <source>Detect QR and barcodes</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="163"/>
        <source>Crop image</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="169"/>
        <source>Open file (browse images)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="171"/>
        <source>Show map explorer</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="173"/>
        <source>Filter images in folder</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="175"/>
        <source>Advanced image sort (Setup)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="177"/>
        <source>Advanced image sort (Quickstart)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="179"/>
        <source>Start Slideshow (Setup)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="181"/>
        <source>Start Slideshow (Quickstart)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="187"/>
        <source>Show Context Menu</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="189"/>
        <source>Hide/Show main menu</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="191"/>
        <source>Hide/Show metadata</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="193"/>
        <source>Hide/Show thumbnails</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="195"/>
        <source>Show floating navigation buttons</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="197"/>
        <source>Toggle fullscreen mode</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="199"/>
        <source>Close window (hides to system tray if enabled)</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="201"/>
        <source>Quit PhotoQt</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="203"/>
        <source>Hide/Show quick actions</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="210"/>
        <source>Show Settings</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="212"/>
        <source>About PhotoQt</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="214"/>
        <source>Show log/debug messages</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="216"/>
        <source>Reset current session</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="218"/>
        <source>Reset current session and hide window</source>
        <extracomment>Name of shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="257"/>
        <source>Shortcuts are grouped by key combination. Multiple actions can be set for each group of key combinations, with the option of cycling through them one by one, or executing all of them at the same time. When cycling through them one by one, a timeout can be set after which the cycle will be reset to the beginning. Any group that has no key combinations set will be deleted when saving all changes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="263"/>
        <source>Add new shortcuts group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="283"/>
        <source>The shortcuts can be filtered by either the key combinations, shortcut actions, category, or all three. For the string search, PhotoQt will by default check if any action/key combination includes whatever string is entered. Adding a dollar sign ($) at the start or end of the search term forces a match to be either at the start or the end of a key combination or action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="295"/>
        <source>Filter key combinations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="305"/>
        <source>Filter shortcut actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="315"/>
        <source>Show all categories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="316"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="317"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="318"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="319"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="320"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="321"/>
        <source>Category:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="423"/>
        <source>no key combination set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="511"/>
        <source>Click to change key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="522"/>
        <source>Click to delete key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="556"/>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="839"/>
        <source>ADD</source>
        <extracomment>Written on small button, used as in: add new key combination. Please keep short!
----------
Written on small button, used as in: add new shortcut action. Please keep short!</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="568"/>
        <source>Click to add new key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="624"/>
        <source>The new shortcut was in use by another shortcuts group. It has been reassigned to this group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="635"/>
        <source>Undo reassignment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="713"/>
        <source>no action selected</source>
        <extracomment>The action here is a shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="763"/>
        <source>unknown:</source>
        <extracomment>The unknown here refers to an unknown internal action that was set as shortcut</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="765"/>
        <source>external</source>
        <extracomment>This is an identifier in the shortcuts settings used to identify an external shortcut.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="768"/>
        <source>quit after</source>
        <extracomment>This is used for listing external commands for shortcuts, showing if the quit after checkbox has been checked</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="778"/>
        <source>Click to change shortcut action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="804"/>
        <source>Click to delete shortcut action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="851"/>
        <source>Click to add new action</source>
        <extracomment>The action here is a shortcut action</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="900"/>
        <source>cycle through actions one by one</source>
        <extracomment>The actions here are shortcut actions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="920"/>
        <source>timeout for resetting cycle:</source>
        <extracomment>The cycle here is the act of cycling through shortcut actions one by one</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/settingsmanager/settings/shortcuts/PQShortcuts.qml" line="967"/>
        <source>run all actions at once</source>
        <extracomment>The actions here are shortcut actions</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/elements/PQSetting.qml" line="150"/>
        <source>reset to default values</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>slideshow</name>
    <message>
        <location filename="../qml/actions/popout/PQSlideshowSetupPopout.qml" line="31"/>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="41"/>
        <source>Slideshow setup</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/popout/PQSlideshowControlsPopout.qml" line="33"/>
        <source>Slideshow</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="545"/>
        <source>Slideshow started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="44"/>
        <source>Start slideshow</source>
        <extracomment>Written on a clickable button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="82"/>
        <source>interval</source>
        <extracomment>The interval between images in a slideshow</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="113"/>
        <source>animation</source>
        <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="125"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="123"/>
        <source>Ken Burns effect</source>
        <extracomment>A special slideshow effect: https://en.wikipedia.org/wiki/Ken_Burns_effect</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="127"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="125"/>
        <source>opacity</source>
        <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="129"/>
        <source>along x-axis</source>
        <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="131"/>
        <source>along y-axis</source>
        <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="133"/>
        <source>rotation</source>
        <extracomment>This is referring to the in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="135"/>
        <source>explosion</source>
        <extracomment>This is referring to the in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="137"/>
        <source>implosion</source>
        <extracomment>This is referring to the in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="139"/>
        <source>choose one at random</source>
        <extracomment>This is referring to the in/out animation of images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="160"/>
        <source>animation speed</source>
        <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="177"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="154"/>
        <source>slow</source>
        <extracomment>Used as in: slow animation</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="191"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="165"/>
        <source>immediately, without animation</source>
        <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="194"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="168"/>
        <source>pretty fast animation</source>
        <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="197"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="171"/>
        <source>not too fast and not too slow</source>
        <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="200"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="174"/>
        <source>very slow animation</source>
        <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="207"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="181"/>
        <source>fast</source>
        <extracomment>Used as in: fast animation</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="216"/>
        <location filename="../qml/settingsmanager/settings/other/PQSlideshow.qml" line="192"/>
        <source>current speed</source>
        <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="233"/>
        <source>looping</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="246"/>
        <source>loop over all files</source>
        <extracomment>Loop over all images during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="262"/>
        <source>shuffle</source>
        <extracomment>during slideshows shuffle the order of all images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="275"/>
        <source>shuffle all files</source>
        <extracomment>during slideshows shuffle the order of all images</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="291"/>
        <source>subfolders</source>
        <extracomment>also include images in subfolders during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="304"/>
        <source>include images in subfolders</source>
        <extracomment>also include images in subfolders during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="321"/>
        <source>status info</source>
        <extracomment>What to do with the file details during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="334"/>
        <source>hide status info during slideshow</source>
        <extracomment>What to do with the file details during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="351"/>
        <source>window buttons</source>
        <extracomment>What to do with the window buttons during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="364"/>
        <source>hide window buttons during slideshow</source>
        <extracomment>What to do with the window buttons during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="381"/>
        <source>music</source>
        <extracomment>The music that is to be played during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQSlideshowSetup.qml" line="398"/>
        <source>enable music</source>
        <extracomment>Enable music to be played during slideshows</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="150"/>
        <source>Click to go to the previous image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="184"/>
        <source>Click to pause slideshow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="185"/>
        <source>Click to play slideshow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="220"/>
        <source>Click to go to the next image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQSlideshowControls.qml" line="252"/>
        <source>Click to exit slideshow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="160"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="162"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="164"/>
        <source>Edit with %1</source>
        <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="166"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="168"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="170"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="172"/>
        <location filename="../cplusplus/singletons/scripts/pqc_scriptscontextmenu.cpp" line="174"/>
        <source>Open in %1</source>
        <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>statusinfo</name>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="193"/>
        <source>Click and drag to move window around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="194"/>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="240"/>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="371"/>
        <source>Click and drag to move status info around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="267"/>
        <source>Click to remove filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="331"/>
        <source>Filter:</source>
        <extracomment>This refers to the currently set filter</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="421"/>
        <source>Connected to:</source>
        <extracomment>Used in tooltip for the chromecast icon</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="430"/>
        <source>Click anywhere to open a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQStatusInfo.qml" line="550"/>
        <source>unknown color profile</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>streaming</name>
    <message>
        <location filename="../qml/actions/popout/PQChromeCastManagerPopout.qml" line="31"/>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="40"/>
        <source>Streaming (Chromecast)</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="46"/>
        <source>Connect</source>
        <extracomment>Used as in: Connect to chromecast device</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="57"/>
        <source>Disconnect</source>
        <extracomment>Used as in: Disconnect from chromecast device</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="132"/>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="157"/>
        <source>No devices found</source>
        <extracomment>The devices here are chromecast devices</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="150"/>
        <source>Status:</source>
        <extracomment>The status refers to whether the chromecast manager is currently scanning or idle</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="154"/>
        <source>Looking for Chromecast devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQChromeCastManager.qml" line="156"/>
        <source>Select which device to connect to</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>thumbnails</name>
    <message>
        <location filename="../qml/filedialog/PQFileView.qml" line="1246"/>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="664"/>
        <source>Reload thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="161"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="511"/>
        <source>File size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/ongoing/PQThumbnails.qml" line="512"/>
        <source>File type:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>unavailable</name>
    <message>
        <location filename="../qml/manage/PQLoader.qml" line="78"/>
        <location filename="../qml/manage/PQLoader.qml" line="81"/>
        <location filename="../qml/other/PQShortcuts.qml" line="661"/>
        <source>Feature unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/manage/PQLoader.qml" line="78"/>
        <source>The chromecast feature is not available in this build of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/manage/PQLoader.qml" line="81"/>
        <source>The location feature is not available in this build of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/other/PQShortcuts.qml" line="661"/>
        <source>Photo spheres are not supported by this build of PhotoQt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="339"/>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="342"/>
        <source>Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="339"/>
        <source>This file type does not support face tags.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/image/components/PQFaceTagger.qml" line="342"/>
        <source>Faces cannot be tagged when inside photo sphere.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>wallpaper</name>
    <message>
        <location filename="../qml/actions/popout/PQWallpaperPopout.qml" line="31"/>
        <location filename="../qml/actions/PQWallpaper.qml" line="43"/>
        <source>Wallpaper</source>
        <extracomment>Window title</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQWallpaper.qml" line="48"/>
        <source>Set as Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQWallpaper.qml" line="99"/>
        <location filename="../qml/actions/PQWallpaper.qml" line="115"/>
        <location filename="../qml/actions/PQWallpaper.qml" line="131"/>
        <location filename="../qml/actions/PQWallpaper.qml" line="147"/>
        <location filename="../qml/actions/PQWallpaper.qml" line="163"/>
        <source>Click to choose %1</source>
        <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/PQWallpaper.qml" line="165"/>
        <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="58"/>
        <source>Other</source>
        <extracomment>Used as in: Other Desktop Environment</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="73"/>
        <source>Warning: %1 module not activated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="81"/>
        <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="69"/>
        <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="72"/>
        <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="80"/>
        <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="73"/>
        <source>Warning: %1 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="101"/>
        <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="93"/>
        <source>Set to which screens</source>
        <extracomment>As in: Set wallpaper to which screens</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="116"/>
        <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="107"/>
        <source>Screen</source>
        <extracomment>Used in wallpaper element</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="139"/>
        <source>Set to which workspaces</source>
        <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQEnlightenment.qml" line="155"/>
        <source>Workspace:</source>
        <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQGnome.qml" line="81"/>
        <location filename="../qml/actions/wallpaperparts/PQWindows.qml" line="63"/>
        <location filename="../qml/actions/wallpaperparts/PQXfce.qml" line="130"/>
        <source>Choose picture option</source>
        <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQOther.qml" line="98"/>
        <source>Tool:</source>
        <extracomment>Tool refers to a program that can be executed</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/actions/wallpaperparts/PQPlasma.qml" line="65"/>
        <source>The image will be set to all screens at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
